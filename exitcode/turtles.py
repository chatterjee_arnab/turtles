bad_local_system_time  = 1
net_address_bad_format = 2
listening_port_in_use  = 3
empty_file_unwriteable = 4
file_size_is_not_zero  = 5
bad_command_invocation = 6
command_does_not_exist = 7

offset = 120

bad_syntax = 6
clean_exit = -120
