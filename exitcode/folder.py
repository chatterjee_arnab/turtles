file_does_not_exist    =  1
Nempty_file_unreadable =  2
hash_type_is_not_valid =  3
import_file_corruption =  4
documentid_not_found   =  5
Woffset_is_not_valid   =  6
Woffset_out_of_range   =  7
wordcount_is_not_valid =  8
list_file_bad_format   =  9
successor_not_allowed  = 10
userid_is_not_valid    = 11
consumption_not_found  = 12
consumption_unreadable = 13
consumption_bad_format = 14

offset = 30
