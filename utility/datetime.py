from datetime       import datetime as _datetime, timezone
from utility.bitops import get, put

class datetime(_datetime):

	@staticmethod
	def __new__(cls, year, month, day, hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone.utc, *, fold=0):
		return _datetime.__new__(cls, year, month, day, hour, minute, second, microsecond, tzinfo, fold=fold)

	def __str__(self):
		self = datetime(self.year, self.month, self.day, self.hour, self.minute, self.second)
		return "'" + super().__str__()[:-6] + "'"

	@staticmethod
	def decode(bytes):
		year, month , day    = get(bytes, 0, 12), get(bytes, 12, 4), get(bytes, 16, 5)
		hour, minute, second = get(bytes, 21, 5), get(bytes, 26, 6), bytes[4]
		return datetime(year + 2021, month, day, hour, minute, second)

	def encode(self):
		barray = bytearray(5)
		put((self.year-2021 << 4).to_bytes(2, "big"), 0, 12, barray)
		put((self.month  << 4,), 12, 4, barray)
		put((self.day    << 3,), 16, 5, barray)
		put((self.hour   << 3,), 21, 5, barray)
		put((self.minute << 2,), 26, 6, barray)
		barray[4] = self.second
		return barray

	@staticmethod
	def now(tz=timezone.utc):
		self = _datetime.now(tz)
		return  datetime(self.year, self.month, self.day, self.hour, self.minute, self.second, self.microsecond, tz, fold=self.fold)

	@staticmethod
	def strptime(date_string, format="%Y-%m-%d %H:%M:%S"):
		self = _datetime.strptime(date_string, format)
		return  datetime(self.year, self.month, self.day, self.hour, self.minute, self.second)

	@staticmethod
	def utcnow():
		self = _datetime.now(timezone.utc)
		return  datetime(self.year, self.month, self.day, self.hour, self.minute, self.second)
