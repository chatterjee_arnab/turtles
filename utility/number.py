from utility.bitops import get, put

def compress(value, maxlen):
	if value < 128: return bytearray((value,))
	bitlen = value.bit_length()
	if maxlen and bitlen > 7*maxlen + 1: return
	length = (bitlen-1 >> 3) + 1
	bitpos, bytes = (length << 3) - bitlen, value.to_bytes(length, "big")
	length = (bitlen - 1)//7 + 1
	if length > maxlen: length = maxlen
	barray = bytearray(length)
	barray[-1] = bytes[-1]
	if length != maxlen: barray[-1] &= 127
	else: bitlen -= 1
	if bitlen == 7 * length: index = 0
	else:
		mod = bitlen % 7
		barray[0], index = 128 | get(bytes, bitpos, mod), 1
		bitpos += mod
	for index in range(index, length-1):
		barray[index] = 128 | get(bytes, bitpos, 7)
		bitpos += 7
	return barray

def decompress(bytes, maxlen):
	if len(bytes) is 1: return bytes[0]
	length = len(bytes)
	if length > maxlen: return
	mod = (bytes[0] & 127).bit_length()
	bitlen, byte = 7 * (length - 1) + mod, bytearray(1)
	if length == maxlen: bitlen += 1
	length = (bitlen-1 >> 3) + 1
	bitpos, barray = (length << 3) - bitlen, bytearray(length)
	barray[-1] = bytes[-1]
	byte[0] = bytes[0] << 8-mod & 255
	put(byte, bitpos, mod, barray)
	bitpos += mod
	for index in range(1, len(bytes) - 1):
		byte[0] = (bytes[index] & 127) << 1
		put(byte, bitpos, 7, barray)
		bitpos += 7
	return int.from_bytes(barray, "big")
