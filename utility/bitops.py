def get(bytes, bitpos, bitlen=1):
	data = 0
	for bitpos in range(bitpos, bitpos + bitlen): data = data<<1 | (bytes[bitpos >> 3] >> 7-(bitpos & 7) & 1)
	return data

def put(bytes, bitpos, bitlen=1, barray=None):
	if barray:
		shortage = (bitpos+bitlen-1 >> 3) + 1 - len(barray)
		if shortage > 0: barray.extend(bytearray(shortage))
	else: barray = bytearray((bitpos+bitlen-1 >> 3) + 1)
	offset = bitpos
	for bitpos in range(bitpos, bitpos + bitlen): barray[bitpos >> 3] |= get(bytes, bitpos - offset) << 7-(bitpos & 7)
	return barray
