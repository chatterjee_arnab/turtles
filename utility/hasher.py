from blake3          import blake3 as BLAKE3

from database.header import Header

from hashlib         import blake2b, blake2s as BLAKE2s, sha256 as SHA2, sha3_256 as SHA3

from pygost          import gost34112012256 as GOST, gost341194 as streebog

from utility.bitops import get

def gethash(databytes, hashtype=7):
	if not (0 < hashtype < 8): return
	def BLAKE2b() : return blake2b(digest_size = 32)
	hasher = (GOST.new, streebog.new, SHA2, SHA3, BLAKE2b, BLAKE2s, BLAKE3)[hashtype - 1]()
	hasher.update(databytes)
	return hasher.digest()

def hashtype(blockid, noloop=False):
	while True:
		i0, i1, i2 = blockid[-1], blockid[-2], blockid[-3]
		b0, b1, b2 = get(blockid, i0), get(blockid, i1), get(blockid, i2)
		hashtype = (b2<<2 | b1<<1 | b0) ^ 7
		if hashtype or noloop: return hashtype
		blockid = Header.get_previous(blockid)
