from utility.bitops import get, put

def check(string):
	if   string[0] is 'T': lower = False
	elif string[0] is 't': lower = True
	else return False
	if lower:
		for i, c in enumerate(string[1:]):
			if not (c.isdigit() or c.islower() and c is not 't'): return False
	else:
		for i, c in enumerate(string[1:]):
			if not (c.isdigit() or c.isupper() and c is not 'T'): return False
	c = string[1].lower()
	if c in "rsuv":
		if len(string) is not 104: return False
	elif c not in "0o1ilyz" and len(string) is not 53: return False
	return True

def decode(string):
	if not check(string): return
	string = string.lower()
	def decoding(base32):
		if base32.isdigit(): return ord(base32) - ord('0') << 3
		return ( 80, 88,  96, 104, 112, 120, 128 , 136,  8 , 144, 152,  8 , 160,
		        168, 0 , 176, 184, 192, 200, None, 208, 216, 224, 232, 240, 248)[ord(base32) - ord('a')]
	meta = string[1]
	if   meta  in  "rsuv":
		barray = bytearray(64)
		barray[0], bitpos = decoding(meta)<<3 & 192, 2
	elif meta not in "yz":
		if   meta in "45": barray = bytearray(33)
		elif meta in "67":
			barray = bytearray(33)
			barray[32] = 1
		else: barray = bytearray(32)
		barray[0], bitpos = decoding(meta)<<4 & 128, 1
	for index in range(2, len(string)):
		data = decoding(string[index])
		put((data,), bitpos, 5, barray)
		bitpos += 5
	return barray

def encode(bytes, metadata):
	encoding = "0123456789abcdefghjkmnpqrsuvwxyz"
	if 12 <= metadata <= 13:
		if len(bytes) is not 64: return
		bitpos, length, meta = 2, 104,  24 | bytes[0]>>6
	else:
		if   2 <= metadata <= 3:
			if len(bytes) is not 33: return
			meta = 16 | bytes[32]<<1
		elif 0 <= metadata < 15:
			if len(bytes) is not 32: return
			meta = metadata<<1
		bitpos, length, meta = 1, 53, meta | bytes[0]>>7
	string = bytearray(length)
	string[0], string[1] = ord('t'), ord(encoding[meta])
	for index in range(2, length):
		data = get(bytes, bitpos, 5)
		string[index] = ord(encoding[data])
		bitpos += 5
	return string.decode()
