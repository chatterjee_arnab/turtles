from database     import database

from threading    import Lock

from utility.exit import status

_lock = None

def acquire():
	if _lock: _lock.acquire()

def create():
	global _lock
	if not _lock: _lock = Lock()

def locked(): return _lock and _lock.locked()

def release():
	database.commit()
	if _lock: _lock.release()
