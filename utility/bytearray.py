class bytearray(bytearray):

	@staticmethod
	def str(self):
		if self is None: return "null"
		return bytearray.__str__(self)

	def __init__(self, arg1=None, arg2=None, arg3=None):
		if arg1 is None: super().__init__()
		elif type(arg1) is str:
			if arg3: super().__init__(arg1, arg2, arg3)
			else   : super().__init__(arg1, arg2)
		elif type(arg1) is int and arg2: super().__init__(arg1.to_bytes(arg2, (arg3, "big")[arg3 is None]))
		else: super().__init__(arg1)

	def __str__(self): return "x'" + self.hex() + "'"
