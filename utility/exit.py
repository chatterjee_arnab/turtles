import exitcode.account
import exitcode.block
import exitcode.database
import exitcode.folder
import exitcode.formula
import exitcode.record
import exitcode.transfer
import exitcode.turtles
import exitcode.usage
import exitcode.wallet

import messages.account
import messages.block
import messages.database
import messages.folder
import messages.formula
import messages.password
import messages.record
import messages.transfer
import messages.turtles
import messages.usage
import messages.wallet

from   sys import stderr

def status(command, identifier, prefix=True, suffix=False):
	command, identifier = command.lower(), identifier.strip()
	module = getattr(messages, command, messages.turtles)
	string = getattr(module, identifier, getattr(messages.turtles, identifier, "Some problem has occurred."))
	if prefix: print(getattr(module, "prefix", ""), end=" ", file=stderr)
	print(string, end="", file=stderr)
	if suffix: print("", getattr(module, "suffix", ""), end="", file=stderr)
	print(file=stderr)
	module = getattr(exitcode, command, exitcode.turtles)
	return   getattr(module, identifier, getattr(exitcode.turtles, identifier, 0)) + module.offset
