from blake3          import blake3

from database.users  import Users
from database.wallet import Wallet

from ecdsa           import SigningKey, SECP256k1, VerifyingKey

from getpass         import getpass

from pyaes           import AESModeOfOperationCBC as AES

from utility         import lock
from utility.exit    import status
from utility.hasher  import gethash

def authenticate(userid, prompt="Enter password: "):
	enckey = Wallet.get_enckey(userid)
	if not enckey: return
	while True:
		password = getpass(prompt)
		if password is "": return False
		key = gethash(password.encode())
		private = decrypt(key, enckey)
		private = SigningKey.from_string(private, curve=SECP256k1)
		if private.verifying_key.to_string() == public(userid): break
		status("password", "incorrect")
	return key

def decrypt(key, cipher32):
	aes, cipher32 = AES(key), bytes(cipher32)
	return aes.decrypt(cipher32[:16]) + aes.decrypt(cipher32[16:])

def encrypt(key, plain_32):
	aes = AES(key)
	return aes.encrypt(plain_32[:16]) + aes.encrypt(plain_32[16:])

def generate(key=None, mining=False):
	if not key:
		key = password()
		if not key: return
	private = SigningKey.generate(curve=SECP256k1)
	public  = private.verifying_key.to_string()
	user = Users()
	user.userid = userid(public)
	wallet = Wallet()
	wallet.userid = user.userid
	wallet.enckey = encrypt(key, private.to_string())
	wallet.mining = mining
	lock.acquire()
	user.put()
	wallet.put()
	lock.release()
	return user.userid

def password(prompt="Enter new password: "):
	while True:
		password = getpass(prompt)
		if password is "": return
		if len(password) < 8:
			status("password", "is_short", suffix=True)
			continue
		digit, lower, upper, other = 4 * (False,)
		for character in password:
			if   character.isdigit(): digit = True
			elif character.islower(): lower = True
			elif character.isupper(): upper = True
			else                    : other = True
			if digit and lower and upper and other:
				confirm = getpass("Confirm new password: ")
				if confirm == password: return gethash(password.encode())
				break
		if   not digit: error = "no_digit"
		elif not lower: error = "no_lower"
		elif not upper: error = "no_upper"
		elif not other: error = "no_other"
		else          : error = "mismatch"
		status("password", error, suffix=True)

def public(userid):
	x, p = int.from_bytes(userid[1:], "big"), SECP256k1.curve.p()
	y = pow(x*x*x + 7, p+1 >> 2, p)
	if y&1 != userid[0]: y = p - y
	return x.to_bytes(32, "big") + y.to_bytes(32, "big")

def sign(data, private):
	key = SigningKey.from_string(private, curve=SECP256k1)
	return key.sign(data, hashfunc=blake3)

def userid(public): return bytes((public[-1] & 1,)) + public[:32]

def validate(data, userid, signature):
	key   = VerifyingKey.from_string(public(userid), curve=SECP256k1)
	try   : return key.verify(signature, data, hashfunc=blake3)
	except: return False
