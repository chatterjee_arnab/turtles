from threading import Thread

def _animation(busy):
	print("Please wait", end=' ')
	while busy[0]:
		print('-' , end='\33[D')
		print('/' , end='\33[D')
		print('|' , end='\33[D')
		print('\\', end='\33[D')
	print('\r' , end='\33[K')

def start():
	busy = [True]
	thread = Thread(target=_animation, args=(busy,))
	thread.start()
	return thread, busy

def stop(animator):
	thread, busy = animator
	busy[0] = False
	thread.join()
