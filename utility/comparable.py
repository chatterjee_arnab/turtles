class Comparable:

	def __eq__(self, other, typ):
		if self is other: return True
		if type(self) is not typ or type(other) is not typ: return False
