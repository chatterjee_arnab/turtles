from database.header   import Header

from utility.bytearray import bytearray
from utility.hasher    import gethash, hashtype

def _aggregate(left, hashtype, right):
	lr = int.from_bytes(gethash(left + right, hashtype), "big")
	rl = int.from_bytes(gethash(right + left, hashtype), "big")
	product = (lr * rl).to_bytes(64, "big")
	l_r = int.from_bytes(product[:16] + product[48:], "big")
	mid = int.from_bytes(product[16:48], "big")
	return (l_r ^ mid).to_bytes(32, "big")

def _generate_tree(tree, hashtype, begin, end):
	if end - begin is 1: return tree[:end]
	odd = (end - begin) & 1
	for index in range(begin, (end, end-1)[odd], 2):
		l, r = tree[index], tree[index + 1]
		tree[end] = _aggregate(tree[index], hashtype, tree[index + 1])
		end += 1
	if odd:
		index += 2
		tree[end] = _aggregate(tree[index], hashtype, tree[index])
		end += 1
		index -= 1
	return _generate_tree(tree, hashtype, index + 2, end)

def augmented_tree(block, pownonce, hashtype):
	length = len(block)
	if not length: return [gethash(b'', hashtype)]
	tree = (length<<1) * [None]
	tree[0] = gethash(block[0].record, hashtype)
	for index in range(1, length): tree[index] = gethash(block[index].record + pownonce, hashtype)
	return _generate_tree(tree, hashtype, 0, length)

def get_path(tree, leaf, count):
	i, level, path = 0, 0, [None] * (count - 1).bit_length()
	def sibling():
		if index & 1: sibling = tree[level + (index^1)]
		else: sibling = tree[level + (index|1, index)[count - index is 1]]
		nonlocal i
		path[i] = sibling
		i += 1
	for index in range(count):
		if tree[index] == leaf:
			if count is 1: return path
			sibling()
			break
	if not path or not path[0]: return
	while count is not 2:
		level += count
		count = (count + 1) >> 1
		index >>= 1
		sibling()
	return path

def verify_path(record, hash_type, certificate):
	nonce_size = len(certificate) & 31
	if nonce_size is 0: nonce_size = 32
	nonce, merkle_path = certificate[:nonce_size], certificate[nonce_size:]
	digest = gethash(record + nonce, hash_type)
	for node in merkle_path: digest = _aggregate(digest, hash_type, node)
	previous, pownonce = Header.get_previous_pownonce(digest)
	return previous and hashtype(previous) is hash_type and pownonce == nonce
