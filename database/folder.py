from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Folder(Comparable):
	_table = " Folder "

	_filehash = " filehash "
	@property
	def filehash(self): return getattr(self, "_Folder__filehash", None)
	@filehash.setter
	def filehash(self, filehash): self.__filehash = filehash

	_hashtype = " hashtype "
	@property
	def hashtype(self): return getattr(self, "_Folder__hashtype", None)
	@hashtype.setter
	def hashtype(self, hashtype): self.__hashtype = hashtype

	_document = " document "
	@property
	def document(self): return getattr(self, "_Folder__document", None)
	@document.setter
	def document(self, document): self.__document = document

	def __eq__(self, other):
		eq = super().__eq__(other, Folder)
		if eq is not None: return eq
		return self.filehash == other.filehash and self.hashtype == other.hashtype
