from blake3           import blake3

from os               import chmod

from sys              import path

from utility.datetime import datetime
from utility.exit     import status

class cache:

	invariant = int.from_bytes(32 * (255,), "big")
	__path = path[0] + "/.cache"

	@property
	def blockid(cache): return cache.__blockid
	@blockid.setter
	def blockid(cache, blockid): cache.__blockid = blockid

	@property
	def blocktim(cache): return cache.__blocktim
	@blocktim.setter
	def blocktim(cache, blocktim): cache.__blocktim = blocktim

	@property
	def hashwind(cache): return cache.__hashwind
	@hashwind.setter
	def hashwind(cache, hashwind): cache.__hashwind = hashwind

	@property
	def totalcon(cache): return cache.__totalcon
	@totalcon.setter
	def totalcon(cache, totalcon): cache.__totalcon = totalcon

	@property
	def length(cache): return cache.__length
	@length.setter
	def length(cache, length): cache.__length = length

	@property
	def idealsiz(cache): return cache.invariant // cache.__hashwind

	def flush(cache):
		try:
			chmod(cache.__path, 0o600)
			file = open(cache.__path, "rb")
			cached = file.read()
			file.close()
			if len(cached) is not 113: raise ValueError
			hasher = blake3()
			hasher.update(cached[:81])
			if hasher.digest() != cached[81:]: raise ValueError
			cache.blocktim = datetime.decode(cached[0:5])
			if cache.__blocktim > datetime.utcnow(): raise ValueError
		except (IOError, ValueError):
			cached = None
			from database.header import Header
			header = Header.get()
			if not header:
				header = Header()
				header.blockid  = bytes(32 * (0,))
				header.previous = header.blockid
				header.protocol = 0
				header.mrkltree = b''
				header.consumed = 0
				header.mining   = 0
				header.blocksiz = 0
				header.blocktim = datetime(2021, 1, 1)
				header.hashwind = cache.invariant
				header.pownonce = b''
				header.totalcon = 0
				header.miningid = None
				header.length   = 0
				header.longest  = True
				header.put()
			cache.blockid  = header.blockid
			cache.blocktim = header.blocktim
			cache.hashwind = header.hashwind
			cache.totalcon = header.totalcon
			cache.length   = header.length
			if cache.__blocktim > datetime.utcnow(): exit(status("turtles", "bad_local_system_time"))
		if cached:
			cache.blockid  = cached[5:37]
			cache.hashwind = int.from_bytes(cached[37:69], "big")
			cache.totalcon = int.from_bytes(cached[69:77], "big")
			cache.length   = int.from_bytes(cached[77:81], "big")
		file = open(cache.__path, "w")
		file.close()

	def dump(cache):
		cached = cache.__blocktim.encode()   + cache.__blockid + \
		cache.__hashwind.to_bytes(32, "big") + \
		cache.__totalcon.to_bytes( 8, "big") + \
		cache.__length  .to_bytes( 4, "big")
		hasher = blake3()
		hasher.update(cached)
		file = open(cache.__path, "wb")
		file.write(cached + hasher.digest())
		file.close()
		chmod(cache.__path, 0o000)

cache = cache()
