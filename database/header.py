from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable
from utility.datetime   import datetime

class Header(Comparable):
	_table = " Header "

	_blockid = " blockid "
	@property
	def blockid(self): return getattr(self, "_Header__blockid", None)
	@blockid.setter
	def blockid(self, blockid): self.__blockid = blockid

	_previous = " previous "
	@property
	def previous(self): return getattr(self, "_Header__previous", None)
	@previous.setter
	def previous(self, previous): self.__previous = previous

	_protocol = " protocol "
	@property
	def protocol(self): return getattr(self, "_Header__protocol", None)
	@protocol.setter
	def protocol(self, protocol): self.__protocol = protocol

	_mrkltree = " mrkltree "
	@property
	def mrkltree(self): return getattr(self, "_Header__mrkltree", None)
	@mrkltree.setter
	def mrkltree(self, mrkltree): self.__mrkltree = mrkltree

	_consumed = " consumed "
	@property
	def consumed(self): return getattr(self, "_Header__consumed", None)
	@consumed.setter
	def consumed(self, consumed): self.__consumed = consumed

	_mining = " mining "
	@property
	def mining(self): return getattr(self, "_Header__mining", None)
	@mining.setter
	def mining(self, mining): self.__mining = mining

	_blocksiz = " blocksiz "
	@property
	def blocksiz(self): return getattr(self, "_Header__blocksiz", None)
	@blocksiz.setter
	def blocksiz(self, blocksiz): self.__blocksiz = blocksiz

	_blocktim = " blocktim "
	@property
	def blocktim(self): return getattr(self, "_Header__blocktim", None)
	@blocktim.setter
	def blocktim(self, blocktim): self.__blocktim = blocktim

	_hashwind = " hashwind "
	@property
	def hashwind(self): return getattr(self, "_Header__hashwind", None)
	@hashwind.setter
	def hashwind(self, hashwind): self.__hashwind = hashwind

	_pownonce = " pownonce "
	@property
	def pownonce(self): return getattr(self, "_Header__pownonce", None)
	@pownonce.setter
	def pownonce(self, pownonce): self.__pownonce = pownonce

	_totalcon = " totalcon "
	@property
	def totalcon(self): return getattr(self, "_Header__totalcon", None)
	@totalcon.setter
	def totalcon(self, totalcon): self.__totalcon = totalcon

	_miningid = " miningid "
	@property
	def miningid(self): return getattr(self, "_Header__miningid", None)
	@miningid.setter
	def miningid(self, miningid): self.__miningid = miningid

	_length = " length "
	@property
	def length(self): return getattr(self, "_Header__length", None)
	@length.setter
	def length(self, length): self.__length = length

	_longest = " longest "
	@property
	def longest(self): return getattr(self, "_Header__longest", None)
	@longest.setter
	def longest(self, longest): self.__longest = longest

	def __eq__(self, other):
		eq = super().__eq__(other, Header)
		if eq is not None: return eq
		return self.blockid == other.blockid

	_mrklroot = " mid(" + _mrkltree + "from -32) "

	@property
	def hashtype(self): return getattr(self, "_Header__hashtype", None)
	@hashtype.setter
	def hashtype(self, hashtype): self.__hashtype = hashtype

	@property
	def exists(self): return database.exists(Header._table, Header._blockid, self.__blockid)

	@staticmethod
	def get(blockid=None):
		if blockid: clause = Header._blockid + "=" + bytearray.str(blockid)
		else: clause = Header._length + "= (select max(" + Header._length +\
		      ") from" + Header._table + ")and" + Header._longest + "is true"
		database.execute\
		("select" + ",".join((Header._blockid , Header._previous,
			Header._protocol, Header._mrkltree, Header._consumed, Header._mining  ,
			Header._blocksiz, Header._blocktim, Header._hashwind, Header._pownonce,
			Header._totalcon, Header._miningid, Header._length  , Header._longest)) + \
		 "from"   + Header._table + \
		 "where"  + clause)
		data = database.fetchone()
		if not data: return
		header = Header()
		header.blockid  = data[0]
		header.previous = data[1]
		header.protocol = int(data[2])
		header.mrkltree = (len(data[3])>>5) * [None]
		header.consumed = int(data[4])
		header.mining   = int(data[5])
		header.blocksiz = int(data[6])
		header.blocktim = datetime.strptime(data[7].decode())
		header.hashwind = int.from_bytes(data[8], "big")
		header.pownonce = data[9]
		header.totalcon = int(data[10])
		header.miningid = data[11]
		header.length   = int(data[12])
		header.longest  = (False, True)[data[13] is not None]
		for index in range(len(header.mrkltree)):
			offset = index<<5
			header.mrkltree[index] = data[3][offset : offset+32]
		return header

	@staticmethod
	def get_blockid(length):
		database.execute\
		("select" + Header._blockid + \
		 "from"   + Header._table   + \
		 "where"  + Header._length  + "=" + str(length) +\
		 " and"   + Header._longest + "is true")
		blockid = database.fetchone()
		if not blockid: return
		return blockid[0]

	@staticmethod
	def get_certificate(blockid):
		database.execute\
		("select" + ",".join((Header._previous, Header._pownonce, Header._mrkltree)) + \
		 "from"   + Header._table    + \
		 "where"  + Header._blockid  + "=" + bytearray.str(blockid) + \
		 " and"   + Header._longest  + "is true")
		certificate = database.fetchone()
		if not certificate: return
		mrkltree = certificate[2]
		length = len(mrkltree) >> 5
		merkle_tree = length * [None]
		for index in range(length): merkle_tree[index] = mrkltree[index<<5 : (index+1)<<5]
		return certificate[0], certificate[1], merkle_tree

	@staticmethod
	def get_consumed(blockid):
		database.execute\
		("select" + Header._consumed + \
		 "from"   + Header._table    + \
		 "where"  + Header._blockid  + "=" + bytearray.str(blockid))
		consumed = database.fetchone()
		if not consumed: return
		return int(consumed[0])

	@staticmethod
	def get_length(blockid):
		database.execute\
		("select" + Header._length  + \
		 "from"   + Header._table   + \
		 "where"  + Header._blockid + "=" + bytearray.str(blockid))
		length = database.fetchone()
		if not length: return
		return int(length[0])

	@staticmethod
	def get_mining(blockid):
		database.execute\
		("select" + Header._mining  + \
		 "from"   + Header._table   + \
		 "where"  + Header._blockid + "=" + bytearray.str(blockid))
		mining = database.fetchone()
		if not mining: return
		return int(mining[0])

	@staticmethod
	def get_miningid(blockid):
		database.execute\
		("select" + Header._miningid + \
		 "from"   + Header._table    + \
		 "where"  + Header._blockid  + "=" + bytearray.str(blockid))
		miningid = database.fetchone()
		if not miningid: return
		return miningid[0]

	@staticmethod
	def get_next(blockid):
		database.execute\
		("select" + ",".join((Header._blockid,
			Header._protocol, Header._mrklroot, Header._consumed,
			Header._mining  , Header._blocksiz, Header._pownonce))  + \
		 "from"   + Header._table    + \
		 "where"  + Header._previous + "=" + bytearray.str(blockid) + \
		 " and"   + Header._longest  + "is true" + \
		 " and"   + Header._length   + "!= 0")
		next = database.fetchone()
		if not next: return
		header = Header()
		header.blockid  = next[0]
		header.protocol = int(next[1])
		header.mrkltree = [next[2]]
		header.consumed = int(next[3])
		header.mining   = int(next[4])
		header.blocksiz = int(next[5])
		header.pownonce = next[6]
		return header

	@staticmethod
	def get_previous_pownonce(mrklroot):
		database.execute\
		("select" + ",".join((Header._previous, Header._pownonce)) + \
		 "from"   + Header._table    + \
		 "where"  + Header._mrklroot + "=" + bytearray.str(mrklroot))
		predecessor = database.fetchall()
		if not predecessor: return
		return predecessor[0]

	@staticmethod
	def get_previous(blockid):
		database.execute\
		("select" + Header._previous + \
		 "from"   + Header._table    + \
		 "where"  + Header._blockid  + "=" + bytearray.str(blockid))
		previous = database.fetchone()
		if not previous: return
		return previous[0]

	@staticmethod
	def get_totalcon(blockid):
		database.execute\
		("select" + Header._totalcon + \
		 "from"   + Header._table    + \
		 "where"  + Header._blockid  + "=" + bytearray.str(blockid))
		totalcon = database.fetchone()
		if not totalcon: return
		return int(totalcon[0])

	def put(self):
		if self.exists: return
		database.execute\
		("insert into" + Header._table + "(" + ",".join((Header._blockid, Header._previous,
			Header._protocol, Header._mrkltree, Header._consumed, Header._mining  ,
			Header._blocksiz, Header._blocktim, Header._hashwind, Header._pownonce,
			Header._totalcon, Header._miningid, Header._length  , Header._longest)) + \
		 ") values (" + ", ".join((\
		bytearray.str(self.__blockid ),
		bytearray.str(self.__previous),
		str(self.__protocol),
		bytearray.str(b''.join(self.__mrkltree)),
		str(self.__consumed),
		str(self.__mining),
		str(self.__blocksiz),
		str(self.__blocktim),
		bytearray.str(self.__hashwind.to_bytes(32, "big")),
		bytearray.str(self.__pownonce),
		str(self.__totalcon),
		bytearray.str(self.__miningid),
		str(self.__length),
		("null", "true")[self.__longest])) + ")")
		return True

	def set_longest(self):
		if not self.exists: return
		database.execute\
		("update" + Header._table   + \
		 "set"    + Header._longest + "=" + ("null ", "true ")[self.__longest] + \
		 "where"  + Header._blockid + "=" + bytearray.str(self.__blockid))
		return True

	def delete(self):
		if not self.exists: return
		database.execute\
		("delete from" +  Header._table  + \
		 "where" + Header._blockid + "=" + bytearray.str(self.__blockid))
		return True
