from database.cache      import cache
from database.connection import MySQLConnection

from mysql.connector     import Error, errorcode

from utility.bytearray   import bytearray
from utility.exit        import status

_connection, _cursor = 2 * (None,)
def cursor():
	cursor = None
	while not cursor: cursor = _cursor
	return cursor

def commit(): return cursor().commit()

def connect():
	global _cursor
	if _cursor: return
	credentials = {"host":"localhost", "user":"", "password":"", "database":"turtles"}
	try:
		global _connection
		_connection = MySQLConnection(**credentials)
		_cursor = _connection.cursor()
	except Error as error:
		if error.errno == errorcode.ER_ACCESS_DENIED_ERROR: exit(status("database", "access_denied"))
		if error.errno == errorcode.ER_BAD_DB_ERROR:        exit(status("database", "doesnot_exist"))
		exit(status("database", "no_connection"))
	cache.flush()

def disconnect():
	cursor().close()
	global _connection, _cursor
	_connection, _cursor = 2 * (None,)
	cache.dump()

def execute(operation, params=None, multi=False): return cursor().execute(operation, params, multi)

def exists(table, primary, value):
	cursor().execute\
	("select   null "  + \
	 "from"  + table   + \
	 "where" + primary + "=" + bytearray.str(value))
	return cursor().fetchone() is not None

def fetchall(): return cursor().fetchall()

def fetchone(): return cursor().fetchone()

def rollback(): return cursor().rollback()
