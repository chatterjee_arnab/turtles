from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class UTXO(Comparable):
	_table = "UTXO"

	_txn = "txn"
	@property
	def txn(self): return getattr(self, "_UTXO__txn", None)
	@txn.setter
	def txn(self, txn): self.__txn = txn

	_oicindex = "oicindex"
	@property
	def oicindex(self): return getattr(self, "_UTXO__oicindex", None)
	@oicindex.setter
	def oicindex(self, oicindex): self.__oicindex = oicindex

	_oictype = "oictype"
	@property
	def oictype(self): return getattr(self, "_UTXO__oictype", None)
	@oictype.setter
	def oictype(self, oictype): self.__oictype = oictype

	def __eq__(self, other):
		eq = super().__eq__(other, UTXO)
		if eq is not None: return eq
		return self.txn == other.txn and self.oicindex == other.oicindex and self.oictype == other.oictype
