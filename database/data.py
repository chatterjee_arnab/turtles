from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Data(Comparable):
	_table = " Data "

	_axsdata = " axsdata "
	@property
	def axsdata(self): return getattr(self, "_Data__axsdata", None)
	@axsdata.setter
	def axsdata(self, axsdata): self.__axsdata = axsdata

	_dsegment = " dsegment "
	@property
	def dsegment(self): return getattr(self, "_Data__dsegment", None)
	@dsegment.setter
	def dsegment(self, dsegment): self.__dsegment = dsegment

	_length = " length "
	@property
	def length(self): return getattr(self, __length, None)
	@length.setter
	def length(self, length): self.__length = length

	def __eq__(self, other):
		eq = super().__eq__(other, Data)
		if eq is not None: return eq
		return self.axsdata == other.axsdata
