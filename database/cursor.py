from mysql.connector.cursor import MySQLCursorRaw

from threading              import Lock

from utility.exit           import status

class MySQLCursorRaw(MySQLCursorRaw):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.__lock = Lock()

	def close(self):
		self.__lock.acquire()
		while self._have_unread_result(): pass
		if self._connection: self._connection.disconnect()
		return super().close()

	def commit(self):
		self.__lock.acquire()
		while self._have_unread_result(): pass
		if self._connection: self._connection.commit()
		self.__lock.release()

	def execute(self, operation, params=None, multi=False):
		self.__lock.acquire()
		while self._have_unread_result(): pass
		try: iterator = super().execute(operation, params, multi)
		except: exit(status("database", ("write", "read")[operation.strip().lower().startswith("select")] + "_failed"))
		self.__lock.release()
		return iterator

	def rollback(self):
		if self._connection: self._connection.rollback()
