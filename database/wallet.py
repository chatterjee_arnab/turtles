from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Wallet(Comparable):
	_table = " Wallet "

	_userid = " userid "
	@property
	def userid(self): return getattr(self, "_Wallet__userid", None)
	@userid.setter
	def userid(self, userid): self.__userid = userid

	_enckey = " enckey "
	@property
	def enckey(self): return getattr(self, "_Wallet__enckey", None)
	@enckey.setter
	def enckey(self, enckey): self.__enckey = enckey

	_mining = " mining "
	@property
	def mining(self): return getattr(self, "_Wallet__mining", None)
	@mining.setter
	def mining(self, mining): self.__mining = mining

	def __eq__(self, other):
		eq = super().__eq__(other, Wallet)
		if eq is not None: return eq
		return self.userid == other.userid

	@property
	def exists(self): return database.exists(Wallet._table, Wallet._userid, self.__userid)

	@staticmethod
	def get_all_mining():
		database.execute\
		("select" + Wallet._userid + \
		 "from"   + Wallet._table  + \
		 "where"  + Wallet._mining + "is true")
		userids = database.fetchall()
		for index in range(len(userids)): userids[index] = userids[index][0]
		return userids

	@staticmethod
	def get_enckey(userid):
		database.execute\
		("select" + Wallet._enckey + \
		 "from"   + Wallet._table  + \
		 "where"  + Wallet._userid + "=" + bytearray.str(userid))
		enckey = database.fetchone()
		if not enckey: return
		return enckey[0]

	@staticmethod
	def get_userids():
		database.execute\
		("select" + Wallet._userid + \
		 "from"   + Wallet._table)
		userids = database.fetchall()
		for index in range(len(userids)): userids[index] = userids[index][0]
		return userids

	def put(self):
		if self.exists: return
		database.execute\
		("insert into" + Wallet._table + "(" + ",".join((Wallet._userid, Wallet._enckey, Wallet._mining)) + \
		 ") values ("  + ", ".join((bytearray.str(self.__userid), bytearray.str(self.__enckey), str(self.__mining))) + ")")
		return True
