from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Owned(Comparable):
	_table = " Owned "

	_accnum = " accnum "
	@property
	def accnum(self): return getattr(self, "_Owned__accnum", None)
	@accnum.setter
	def accnum(self, accnum): self.__accnum = accnum

	_balance  = " balance "
	@property
	def balance(self): return getattr(self, "_Owned__balance", None)
	@balance.setter
	def balance(self, balance): self.__balance = balance

	def __eq__(self, other):
		eq = super().__eq__(other, Owned)
		if eq is not None: return eq
		return self.accnum == other.accnum
