from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Transfer(Comparable):
	_table = " Transfer "

	_txn = " txn "
	@property
	def txn(self): return getattr(self, "_Transfer__txn", None)
	@txn.setter
	def txn(self, txn): self.__txn = txn

	_payee = " payee "
	@property
	def payee(self): return getattr(self, "_Transfer__payee", None)
	@payee.setter
	def payee(self, payee): self.__payee = payee

	_amount = " amount "
	@property
	def amount(self): return getattr(self, "_Transfer__amount", None)
	@amount.setter
	def amount(self, amount): self.__amount = amount

	@property
	def verifier(self): return getattr(self, "_Transfer__verifier", None)
	@verifier.setter
	def verifier(self, verifier): self.__verifier = verifier

	def __eq__(self, other):
		eq = super().__eq__(other, Transfer)
		if eq is not None: return eq
		return self.txn == other.txn

	@property
	def exists(self): return database.exists(Transfer._table, Transfer._txn, self.__txn)

	@staticmethod
	def get(txn):
		database.execute\
		("select" + ",".join((Transfer._payee, Transfer._amount)) + \
		 "from"   + Transfer._table + \
		 "where"  + Transfer._txn   + "=" + bytearray.str(txn))
		data = database.fetchone()
		if not data: return
		transfer = Transfer()
		transfer.txn, transfer.payee, transfer.amount = txn, data[0], int(data[1])
		return transfer

	@staticmethod
	def get_by_payee(payee):
		database.execute\
		("select" + ",".join((Transfer._txn, Transfer._amount)) + \
		 "from"   + Transfer._table + \
		 "where"  + Transfer._payee + "=" + bytearray.str(payee))
		transfers = database.fetchall()
		for index, data in enumerate(transfers):
			transfer = Transfer()
			transfers[index] = transfer
			transfer.txn, transfer.payee, transfer.amount = data[0], payee, int(data[1])
		return transfers

	def put(self):
		if self.exists: return
		database.execute\
		("insert into" + Transfer._table + "(" + ",".join((Transfer._txn, Transfer._payee, Transfer._amount)) + \
		 ") values ("  + ", ".join((bytearray.str(self.__txn), bytearray.str(self.__payee), str(self.__amount))) + ")")
		return True

	def verified(self, blockid): return self.__verifier(self, blockid)
