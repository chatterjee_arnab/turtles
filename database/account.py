from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Account(Comparable):
	_table = " Account "

	_accnum = " accnum "
	@property
	def accnum(self): return getattr(self, "_Account__accnum", None)
	@accnum.setter
	def accnum(self, accnum): self.__accnum = accnum

	_groupid = " groupid "
	@property
	def groupid(self): return getattr(self, "_Account__groupid", None)
	@groupid.setter
	def groupid(self, groupid): self.__groupid = groupid

	_locking = " locking "
	@property
	def locking(self): return getattr(self, "_Account__locking", None)
	@locking.setter
	def locking(self, locking): self.__locking = locking

	_plenum = " plenum "
	@property
	def plenum(self): return getattr(self, "_Account__plenum", None)
	@plenum.setter
	def plenum(self, plenum): self.__plenum = plenum

	_quorum = " quorum "
	@property
	def quorum(self): return getattr(self, "_Account__quorum", None)
	@quorum.setter
	def quorum(self, quorum): self.__quorum = quorum

	_length = " length "
	@property
	def length(self): return getattr(self, "_Account__length", None)
	@length.setter
	def length(self, length): self.__length = length

	def __eq__(self, other):
		eq = super().__eq__(other, Account)
		if eq is not None: return eq
		return self.accnum == other.accnum and self.groupid == other.groupid
