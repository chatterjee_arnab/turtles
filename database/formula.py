from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Formula(Comparable):
	_table = " Formula "

	_axsform = " axsform "
	@property
	def axsform(self): return getattr(self, "_Formula__axsform", None)
	@axsform.setter
	def axsform(self, axsform): self.__axsform = axsform

	_axscode = " axscode "
	@property
	def axscode(self): return getattr(self, "_Formula__axscode", None)
	@axscode.setter
	def axscode(self, axscode): self.__axscode = axscode

	_axsdata = " axsdata "
	@property
	def axsdata(self): return getattr(self, "_Formula__axsdata", None)
	@axsdata.setter
	def axsdata(self, axsdata): self.__axsdata = axsdata

	_length = " length "
	@property
	def length(self): return getattr(self, "_Formula__length", None)
	@length.setter
	def length(self, length): self.__length = length

	def __eq__(self, other):
		eq = super().__eq__(other, Formula)
		if eq is not None: return eq
		return self.axsform == other.axsform
