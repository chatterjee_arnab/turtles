from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Usergroup(Comparable):
	_table = " Usergroup "

	_groupid = " groupid "
	@property
	def groupid(self): return getattr(self, "_Usergroup__groupid", None)
	@blockid.setter
	def groupid(self, groupid): self.__groupid = groupid

	_userid = " userid "
	@property
	def userid(self): return getattr(self, "_Usergroup__userid", None)
	@userid.setter
	def userid(self, userid): self.__userid = userid

	_length = " length "
	@property
	def length(self): return getattr(self, "_Usergroup__length", None)
	@length.setter
	def length(self, length): self.__length = length

	def __eq__(self, other):
		eq = super().__eq__(other, Usergroup)
		if eq is not None: return eq
		return self.groupid == other.groupid and self.userid == other.userid
