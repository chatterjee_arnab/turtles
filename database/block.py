from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Block(Comparable):
	_table = " Block "

	_blockid = " blockid "
	@property
	def blockid(self): return getattr(self, "_Block__blockid", None)
	@blockid.setter
	def blockid(self, blockid): self.__blockid = blockid

	_offset = " offset "
	@property
	def offset(self): return getattr(self, "_Block__offset", None)
	@offset.setter
	def offset(self, offset): self.__offset = offset

	_recordid = " recordid "
	@property
	def recordid(self): return getattr(self, "_Block__recordid", None)
	@recordid.setter
	def recordid(self, recordid): self.__recordid = recordid

	def __eq__(self, other):
		eq = super().__eq__(other, Block)
		if eq is not None: return eq
		return self.blockid == other.blockid and self.offset == other.offset

	@property
	def exists(self):
		database.execute\
		("select null " + \
		 "from"  + Block._table   + \
		 "where" + Block._blockid + "=" + bytearray.str(self.__blockid) + \
		 "and"   + Block._offset  + "=" + str(self.__offset))
		return database.fetchone() is not None

	@staticmethod
	def get_blockids(recordid):
		database.execute\
		("select" + Block._blockid  + \
		 "from"   + Block._table    + \
		 "where"  + Block._recordid + "=" + bytearray.str(recordid))
		blockids = database.fetchall()
		for index in range(len(blockids)): blockids[index] = blockids[index][0]
		return blockids

	@staticmethod
	def get_count(blockid):
		database.execute\
		("select count(*)" + \
		 "from"  + Block._table    + \
		 "where" + Block._blockid  + "=" + bytearray.str(blockid))
		count = database.fetchone()[0]
		return int(("0", count)[count is not None])

	@staticmethod
	def get_recordids(blockid):
		database.execute\
		("select"   + Block._recordid + \
		 "from"     + Block._table    + \
		 "where"    + Block._blockid  + "=" + bytearray.str(blockid) + \
		 "order by" + Block._offset)
		recordids = database.fetchall()
		for index in range(len(recordids)): recordids[index] = recordids[index][0]
		return recordids

	def put(self):
		if self.exists: return
		database.execute\
		("insert into" + Block._table + "(" + ",".join((Block._blockid, Block._offset, Block._recordid)) + \
		 ") values ("  + ", ".join((bytearray.str(self.__blockid), str(self.__offset), bytearray.str(self.__recordid))) + ")")
		return True
