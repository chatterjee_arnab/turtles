from database.cursor            import MySQLCursorRaw

from mysql.connector.connection import MySQLConnection

from utility.exit               import status

class MySQLConnection(MySQLConnection):

	def commit(self):
		try   : super().commit()
		except: exit(status("database", "commit_failed"))

	def cursor(self): return super().cursor(raw=True, cursor_class=MySQLCursorRaw)
