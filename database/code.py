from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Code(Comparable):
	_table = " Code "

	_axscode = " axscode "
	@property
	def axscode(self): return getattr(self, "_Code__axscode", None)
	@axscode.setter
	def axscode(self, axscode): self.__axscode = axscode

	_csegment = " csegment "
	@property
	def csegment(self): return getattr(self, "_Code__csegment", None)
	@csegment.setter
	def csegment(self, csegment): self.__csegment = csegment

	_length = " length "
	@property
	def length(self): return getattr(self, __length, None)
	@length.setter
	def length(self, length): self.__length = length

	def __eq__(self, other):
		eq = super().__eq__(other, Code)
		if eq is not None: return eq
		return self.axscode == other.axscode
