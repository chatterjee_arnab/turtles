from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Users(Comparable):
	_table = " Users "

	_userid = " userid "
	@property
	def userid(self): return getattr(self, "_Users__userid", None)
	@userid.setter
	def userid(self, userid): self.__userid = userid

	_balance = " balance "
	@property
	def balance(self): return getattr(self, "_Users__balance", None)
	@balance.setter
	def balance(self, balance): self.__balance = balance

	_consumed = " consumed "
	@property
	def consumed(self): return getattr(self, "_Users__consumed", None)
	@consumed.setter
	def consumed(self, consumed): self.__consumed = consumed

	_mining = " mining "
	@property
	def mining(self): return getattr(self, "_Users__mining", None)
	@mining.setter
	def mining(self, mining): self.__mining = mining

	def __eq__(self, other):
		eq = super().__eq__(other, Users)
		if eq is not None: return eq
		return self.userid == other.userid

	@property
	def exists(self): return database.exists(Users._table, Users._userid, self.__userid)

	@staticmethod
	def get_balance(userid):
		database.execute\
		("select" + Users._balance + \
		 "from"   + Users._table   + \
		 "where"  + Users._userid  + "=" + bytearray.str(userid))
		balance = database.fetchone()
		if not balance: return
		return int(balance[0])

	@staticmethod
	def get_consumed(userid):
		database.execute\
		("select" + Users._consumed + \
		 "from"   + Users._table   + \
		 "where"  + Users._userid  + "=" + bytearray.str(userid))
		consumed = database.fetchone()
		if not consumed: return
		return int(consumed[0])

	@staticmethod
	def get_mining(userid):
		database.execute\
		("select" + Users._mining + \
		 "from"   + Users._table  + \
		 "where"  + Users._userid + "=" + bytearray.str(userid))
		mining = database.fetchone()
		if not mining: return
		return int(mining[0])

	def put(self):
		if self.exists: return
		database.execute\
		("insert into" + Users._table + "(" + Users._userid + \
		 ") values ("  + bytearray.str(self.__userid) + ")")
		return True

	def set_balance(self):
		if not self.exists: return
		database.execute\
		("update" + Users._table   + \
		 "set"    + Users._balance + "=" + Users._balance + "+" + str(self.__balance) + " " + \
		 "where"  + Users._userid  + "=" + bytearray.str(self.__userid))
		return True

	def set_consumed(self):
		if not self.exists: return
		database.execute\
		("update" + Users._table    + \
		 "set"    + Users._consumed + "=" + Users._consumed + "+" + str(self.__consumed) + " " + \
		 "where"  + Users._userid   + "=" + bytearray.str(self.__userid))
		return True

	def set_mining(self):
		if not self.exists: return
		database.execute\
		("update" + Users._table  + \
		 "set"    + Users._mining + "=" + Users._mining + "+" + str(self.__mining) + " " + \
		 "where"  + Users._userid + "=" + bytearray.str(self.__userid))
		return True
