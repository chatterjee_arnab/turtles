from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Used(Comparable):
	_table = " Used "

	_transfer = " transfer "
	@property
	def transfer(self): return getattr(self, "_Used__transfer", None)
	@transfer.setter
	def transfer(self, transfer): self.__transfer = transfer

	_usedby = " usedby "
	@property
	def usedby(self): return getattr(self, "_Used__usedby", None)
	@usedby.setter
	def usedby(self, usedby): self.__usedby = usedby

	def __eq__(self, other):
		eq = super().__eq__(other, STXO)
		if eq is not None: return eq
		return self.txn == other.txn and self.usedby == other.usedby

	@property
	def exists(self): return database.exists(Used._table, Used._transfer, self.__transfer)

	@staticmethod
	def get_by_usedby(usedby):
		database.execute\
		("select" + Used._transfer + \
		 "from"   + Used._table    + \
		 "where"  + Used._usedby   + "=" + bytearray.str(usedby))
		transfer = database.fetchone()
		if not transfer: return
		return transfer[0]

	@staticmethod
	def get_usedby(transfer):
		database.execute\
		("select" + Used._usedby   + \
		 "from"   + Used._table    + \
		 "where"  + Used._transfer + "=" + bytearray.str(transfer))
		usedby = database.fetchall()
		for index in range(len(usedby)): usedby[index] = usedby[index][0]
		return usedby

	def put(self):
		if self.exists: return
		database.execute\
		("insert into" + Used._table + "(" + ",".join((Used._transfer, Used._usedby)) + \
		 ") values ("  + ", ".join((bytearray.str(self.__transfer), bytearray.str(self.__usedby))) + ")")
		return True
