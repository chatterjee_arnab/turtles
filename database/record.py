from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Record(Comparable):
	_table = " Record "

	_recordid = " recordid "
	@property
	def recordid(self): return getattr(self, "_Record__recordid", None)
	@recordid.setter
	def recordid(self, recordid): self.__recordid = recordid

	_record = " record "
	@property
	def record(self): return getattr(self, "_Record__record", None)
	@record.setter
	def record(self, record): self.__record = record

	def __eq__(self, other):
		eq = super().__eq__(other, Record)
		if eq is not None: return eq
		return self.recordid == other.recordid

	@property
	def count(self): return getattr(self, "_Record__count", None)
	@count.setter
	def count(self, count): self.__count = count

	@property
	def pownonce(self): return getattr(self, "_Record__pownonce", None)
	@pownonce.setter
	def pownonce(self, pownonce): self.__pownonce = pownonce

	@property
	def transfer(self): return getattr(self, "_Record__transfer", None)
	@transfer.setter
	def transfer(self, transfer): self.__transfer = transfer

	@property
	def exists(self): return database.exists(Record._table, Record._recordid, self.__recordid)

	@staticmethod
	def get(recordid):
		database.execute\
		("select" + Record._record   + \
		 "from"   + Record._table    + \
		 "where"  + Record._recordid + "=" + bytearray.str(recordid))
		record = database.fetchone()
		if not record: return
		return record[0]

	@staticmethod
	def get_size(recordid):
		database.execute\
		("select length(" + Record._record + ")" + \
		 "from"  + Record._table    + \
		 "where" + Record._recordid + "=" + bytearray.str(recordid))
		size = database.fetchone()
		if not size: return
		return int(size[0])

	def put(self):
		if self.exists: return
		database.execute\
		("insert into" + Record._table + "(" + ",".join((Record._recordid, Record._record)) + \
		 ") values ("  + ", ".join((bytearray.str(self.__recordid), bytearray.str(self.__record))) + ")")
		return True
