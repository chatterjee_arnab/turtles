from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Document(Comparable):
	_table = " Document "

	_document = " document "
	@property
	def document(self): return getattr(self, "_Document__document", None)
	@document.setter
	def document(self, document): self.__document = document

	_follower = " follower "
	@property
	def follower(self): return getattr(self, "_Document__follower", None)
	@follower.setter
	def follower(self, follower): self.__follower = follower

	def __eq__(self, other):
		eq = super().__eq__(other, Document)
		if eq is not None: return eq
		return self.document == other.document
