from database           import database

from utility.bytearray  import bytearray
from utility.comparable import Comparable

class Shared(Comparable):
	_table = " Shared "

	_accnum = " accnum "
	@property
	def accnum(self): return getattr(self, "_Shared__accnum", None)
	@accnum.setter
	def accnum(self, accnum): self.__accnum = accnum

	_funds  = " funds "
	@property
	def funds(self): return getattr(self, "_Shared__funds", None)
	@funds.setter
	def funds(self, funds): self.__funds = funds

	def __eq__(self, other):
		eq = super().__eq__(other, Shared)
		if eq is not None: return eq
		return self.accnum == other.accnum
