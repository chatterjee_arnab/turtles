from database.block    import Block
from database.cache    import cache
from database.header   import Header
from database.record   import Record
from database.transfer import Transfer
from database.used     import Used
from database.wallet   import Wallet

from utility.hasher    import gethash
from utility.security  import decrypt, sign, validate

def deserialize(data, blockid):
	payee, txn, signature = data[2:35], data[35:67], data[67:131]
	transfer = Transfer.get(txn)
	if not transfer or transfer.amount < len(data) or not validate(data[:-64], transfer.payee, signature): return
	if not verify(transfer, blockid): return False
	record = Record()
	record.transfer = transfer
	transfer.payee, transfer.verifier = payee, verify
	return record

def serialize(record, userid, key):
	data = b"\x00\x01" + record.transfer.payee + record.transfer.txn
	enckey = Wallet.get_enckey(userid)
	private = decrypt(key, enckey)
	signature = sign(data, private)
	record.record = data + signature
	record.recordid = gethash(record.record)

def verify(transfer, blockid):
	txnblock = _verify(transfer, blockid)
	if not txnblock: return
	usedby = Used.get_usedby(transfer.txn)
	if not usedby: return True
	while blockid != txnblock:
		if blockid in usedby: return
		blockid = Header.get_previous(blockid)
	transfer.verifier = verify
	return True

def _verify(transfer, blockid):
	high = Header.get_length(blockid)
	blockids, low = [], high
	for txnblock in Block.get_blockids(transfer.txn):
		blockids.append(txnblock)
		length = Header.get_length(txnblock)
		if length < low: low = length
	for i in range(low, high+1):
		if blockid in blockids: return blockid
		blockid = Header.get_previous(blockid)
