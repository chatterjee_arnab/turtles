from database.cache   import cache
from database.header  import Header

from datetime         import timedelta

from utility.datetime import datetime
from utility.hasher   import gethash, hashtype
from utility.number   import compress, decompress

def _normalized(window):
	window = int(window)
	if not window: return 1
	if window < cache.invariant: return window
	return cache.invariant

def adjust_window(header):
	previous = header.previous
	if header.consumed and previous.totalcon:
		header.hashwind >>= header.mining
		poc_ratio = header.consumed * previous.length / previous.totalcon
		if poc_ratio < 1: header.hashwind *= poc_ratio

def deserialize(previous, data, base=0):
	error, header, offset = 2 * (None,), Header(), base + 1
	header.previous, header.protocol = previous, 0
	header.mrkltree, header.hashtype = [data[offset : offset+32]], hashtype(previous.blockid, True)
	if not header.hashtype: header.hashtype = previous.hashtype
	metadata = data[offset+32]
	offset += 33
	if metadata&128:
		blocksiz_len = (metadata>>5 & 3) + 1
		header.blocksiz = int.from_bytes(data[offset : offset+blocksiz_len], "big") + 1
		header.mining, consumed = data[offset+blocksiz_len], []
		offset += blocksiz_len + 1
		for offset in range(offset, offset+8):
			consumed.append(data[offset])
			if data[offset] < 128: break
		offset += 1
		header.consumed = decompress(consumed, 8)
		if not header.consumed and previous.totalcon: return error
	elif  header.mrkltree[0] != gethash(b'', header.hashtype): return error
	else: header.blocksiz, header.mining, header.consumed = 3 * (0,)
	nonce_size = (metadata & 31) + 1
	header.pownonce = data[offset : offset+nonce_size]
	offset += nonce_size
	header.blockid  = gethash (previous.blockid + data[base:offset], header.hashtype)
	header.hashwind = previous.hashwind
	adjust_window(header)
	return (error, (header, offset))[proof_of_work(header, set_blocktim(header)) and verify_time(header.blocktim)]

def proof_of_work(header, size_factor):
	if not verify_hash(header, size_factor): return False
	previous = header.previous
	header.hashwind = _normalized(size_factor * previous.hashwind)
	header.length, header.longest = previous.length + 1, False
	if   header.consumed: header.totalcon = previous.totalcon + header.consumed
	elif previous.length: header.totalcon = previous.totalcon + previous.totalcon // previous.length
	else: header.totalcon = 0
	return True

def serialize(header):
	if header.blocksiz: blocksiz_len = header.blocksiz.bit_length()-1 >> 3
	else: blocksiz_len = 0
	metadata = (header.blocksiz is not 0)<<7 | blocksiz_len<<5 | len(header.pownonce)-1
	data = compress(header.protocol, 2) + header.mrkltree[-1] + bytes((metadata,))
	if header.blocksiz: data += (header.blocksiz - 1).to_bytes(blocksiz_len + 1, "big") + bytes((header.mining,)) + compress(header.consumed, 8)
	return data + header.pownonce

def set_blockid(header):
	previous, hdr  = header.previous.blockid, serialize(header)
	header.blockid = gethash(previous + hdr, header.hashtype)
	return hdr

def set_blocktim(header):
	if header.blocksiz:
		ideal_size  = cache.invariant // header.previous.hashwind
		size_factor = header.blocksiz / ideal_size
	else: size_factor = 1/60
	seconds = int(size_factor * 60)
	if not seconds: seconds = 1
	block_interval  = timedelta(0, seconds)
	header.blocktim = header.previous.blocktim + block_interval
	return size_factor

def verify_hash(header, size_factor):
	previous, window = int.from_bytes(header.previous.blockid, "big"), size_factor * header.hashwind
	low, high = (previous + 1) & cache.invariant, (previous + _normalized(window)) & cache.invariant
	blockid = int.from_bytes(header.blockid, "big")
	l, h = low <= blockid, blockid <= high
	return blockid and (l or h, l and h)[low <= high]

def verify_time(blocktim): return blocktim <= datetime.utcnow()
