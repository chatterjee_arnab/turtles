from database.cache    import cache
from database.header   import Header
from database.record   import Record
from database.transfer import Transfer
from database.users    import Users

from utility           import lock
from utility.number    import compress, decompress
from utility.hasher    import gethash

def deserialize(data, blockid=None):
	base,transfer = Record(), Transfer()
	base.transfer = transfer
	count = []
	for offset in range(2, 5):
		count.append(data[offset])
		if data[offset] < 128: break
	transfer.amount = int.from_bytes(data[offset+1 : offset+5], "big") + 1
	if transfer.amount < len(data): return
	base.count, metadata = decompress(count, 3) + 1, data[offset+5]
	payee_flag, miner_y, payee_y = metadata & 128, metadata>>6 & 1, metadata>>5 & 1
	nonce_size = (metadata & 31) + 1
	miner_halfkey = data[offset+6 : offset+38]
	miner = bytes((miner_y,)) + miner_halfkey
	user = Users()
	user.userid = miner
	if not user.exists:
		if Header.get_totalcon(blockid): return
		lock.acquire()
		user.put()
		lock.release()
	if payee_flag:
		payee_halfkey = data[offset+38 : offset+70]
		transfer.payee = bytes((payee_y,)) + payee_halfkey
		offset += 70
	else: offset += 38
	if offset + nonce_size != len(data): return
	base.pownonce, transfer.txn = data[offset:], miner
	return base

def marshal(header, base):
	transfer = base.transfer
	payee = (transfer.txn, transfer.payee)[transfer.payee is not None]
	metadata = (transfer.payee is not None)<<7 | transfer.txn[0]<<6 | payee[0]<<5 | len(base.pownonce)-1
	count, record = compress(base.count - 1, 3), bytes((metadata,)) + transfer.txn[1:]
	if metadata&128: record += payee[1:]
	record += base.pownonce
	header.blocksiz += 6 + len(count) + len(record) - len(base.record)
	transfer.amount = next_idealsiz(header)
	base.record = bytes(2) + count + (transfer.amount - 1).to_bytes(4, "big") + record
	base.recordid = gethash(base.record)

def next_idealsiz(header):
	ideal_size  = cache.invariant // header.previous.hashwind
	size_factor = ideal_size / header.blocksiz
	return int(ideal_size * size_factor)
