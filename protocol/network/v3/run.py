from protocol.network.v3.reply   import Reply
from protocol.network.v3.request import Request
from protocol.network.v3.sync    import synchronize

def main(peer):
	reply = Reply(peer)
	request = Request(peer, reply)
	reply.request = request
	if not synchronize(peer, request): return
	while reply.service() and request.service(): pass
