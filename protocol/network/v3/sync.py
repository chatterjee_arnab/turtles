from connection.peer import Peer

from daemon.miningd  import append

from database.block  import Block
from database.cache  import cache
from database.header import Header

from threading       import Lock

_lock = Lock()

def synchronize(peer, request):
	if not (Peer.lightweight or peer.record_size): return True
	_lock.acquire()
	chain_length = cache.length
	synced, peer.synced = True, chain_length <= peer.chain_length
	while cache.length < peer.chain_length:
		synced = False
		blockid = Header.get_blockid(chain_length)
		headers = request.headers(blockid)
		if headers is None or headers is []: break
		if headers is False:
			if not chain_length: break
			chain_length -= 1
			continue
		for header in headers:
			if header.exists: continue
			if not Peer.lightweight and header.blocksiz:
				block = request.block(header.blockid, header.previous.blockid)
				if not block:
					_lock.release()
					return
			else: block = []
			if append(header, block) is None:
				_lock.release()
				return
		chain_length = cache.length
		if not request.synchronize(): break
		synced = True
	_lock.release()
	return synced
