from connection.address      import Address
from connection.peer         import Peer

from daemon.miningd          import append

from database.cache          import cache
from database.header         import Header
from database.record         import Record

from protocol.network.v3     import tag
from protocol.network.v3.tag import available, unavailable
from protocol.version        import commodity, transfer

from socket                  import AF_INET, AF_INET6, inet_ntop

from threading               import Lock

from utility.bitops          import get
from utility.hasher          import gethash, hashtype
from utility.merkle          import verify_path
from utility.number          import decompress

class Request:

	def __init__(self, peer, reply): self.__peer, self.__reply, self.__lock = peer, reply, Lock()

	def service(self):
		request = self.__peer.request
		if not request: return True
		identifier, data = request
		method = getattr(Request, "_Request__" + identifier, None)
		if method: reply = method(self, data)
		else: reply = False
		if reply is None: return
		self.__peer.reply = reply
		return True

	def   block(self, blockid, previous): return self.__critical_request(Request.__block, blockid, previous)
	def __block(self, blockid, previous):
		if not self.__peer.record_size: return False
		if not self.__request(tag.block, blockid): return False
		socket = self.__peer.socket
		count  = int.from_bytes(socket.recv(3), "big") + 1
		block  = count * [None]
		for index in range(count): block[index] = socket.recv(32)
		for index, recordid in enumerate(block):
			data = Record.get(recordid)
			if data:
				version = data[1]
				record = (commodity, transfer)[version&1](version)[0].main.deserialize(data, blockid)
				record.recordid, record.record = recordid, data
			else:
				record = self.__record(recordid, previous)
				if not record: return
			block[index] = record
		return block

	def __block_broadcast(self, block):
		self.__request(tag.block_broadcast, block)
		return True

	def __certificate(self, record):
		socket = self.__peer.socket
		metadata = socket.recv(2)
		hashtype = get(metadata, 0, 3)
		if hashtype is 0: return
		nonce_size, merkle_len = get(metadata, 3, 5) + 1, get(metadata, 8, 5)
		certificate = socket.recv(nonce_size + (merkle_len << 5))
		return verify_path(record, hashtype, certificate)

	def   headers(self, blockid): return self.__critical_request(Request.__headers, blockid)
	def __headers(self, blockid):
		if not self.__request(tag.headers, blockid): return False
		socket = self.__peer.socket
		count  = int.from_bytes(socket.recv(4), "big") + 1
		size   = int.from_bytes(socket.recv(5), "big") + 1
		data   = socket.recv(size)
		headers, offset, previous = count * [None], 0, Header.get(blockid)
		previous.hashtype = hashtype(previous.previous)
		for index in range(count):
			if offset == size: return headers[:index]
			if data[offset] < 128: version = data[offset]
			elif size - offset is 1: return headers[:index]
			else: version = decompress(data[offset : offset+2], 2)
			version = commodity(version)[0]
			if not version: return headers[:index]
			header,offset = version.header.deserialize(previous, data, offset)
			if not header : return headers[:index]
			headers[index], previous = 2 * (header,)
		return headers

	def __peers(self, exclude):
		filters = {None:243, AF_INET:115, AF_INET6:179}[exclude]
		if not self.__request(tag.peers, bytes((filters, 30))): return False
		socket = self.__peer.socket
		count = socket.recv(1)[0]
		if not count: return
		IPv4, IPv6 = count>>4, count&15
		if {None:0, AF_INET:IPv4, AF_INET6:IPv6}[exclude] != 0: return
		addresses = (IPv4 + IPv6) * [None]
		if IPv4:
			peers = socket.recv(14 * IPv4)
			for index in range(IPv4):
				offset = 14 * index
				host, port = peers[offset : offset+4], peers[offset+4 : offset+6]
				if not (port[0] or port[1]): return
				fresh = int.from_bytes(peers[offset+ 6 : offset+10], "big")
				total = int.from_bytes(peers[offset+10 : ], "big")
				if total is 0: return
				host, port = inet_ntop(AF_INET, host), int.from_bytes(port, "big")
				address = Address(host, port, fresh, total)
				if address in addresses: return
				addresses[index] = address
		if IPv6:
			peers = socket.recv(26 * IPv6)
			for index in range(IPv6):
				offset = 26 * index
				host, port = peers[offset : offset+16], peers[offset+16 : offset+18]
				if not (port[0] or port[1]): continue
				fresh = int.from_bytes(peers[offset+18 : offset+22], "big")
				total = int.from_bytes(peers[offset+22 : ], "big")
				if total is 0: return
				host, port = inet_ntop(AF_INET6, host), int.from_bytes(port, "big")
				address = Address(host, port, fresh, total)
				if address in addresses: return
				addresses[IPv4 + index] = address
		return addresses

	def   record(self, recordid, blockid=None, verify=False, broadcast=False):
	      return self.__critical_request(Request.__record, recordid, blockid, verify, broadcast)
	def __record(self, recordid, blockid=None, verify=False, broadcast=False):
		if not self.__peer.record_size: return False
		code = (tag.record, tag.record_certified)[Peer.lightweight]
		if not self.__request(code, recordid): return False
		socket = self.__peer.socket
		size = int.from_bytes(socket.recv(3), "big") + 1
		if broadcast and size > Peer.record_max: return
		data = socket.recv(size)
		if data[0] or gethash(data) != recordid or Peer.lightweight and not self.__certificate(data): return
		version = data[1]
		protocol, deprecated = (commodity, transfer)[version&1](version)
		if not protocol or deprecated and verify: return
		record = protocol.main.deserialize(data, blockid)
		if not record: return record
		record.recordid, record.record = recordid, data
		return record

	def __record_broadcast(self, record):
		if len(record) > self.__peer.record_size: return False
		self.__request(tag.record_broadcast, gethash(record))
		return True

	def   synchronize(self): return self.__critical_request(Request.__synchronize)
	def __synchronize(self):
		if not self.__request(tag.synchronize, cache.length.to_bytes(4, "big")): return
		peer = self.__peer
		chain_length = int.from_bytes(peer.socket.recv(4), "big")
		if chain_length < peer.chain_length: return
		peer.chain_length = chain_length
		return True

	def __critical_request(self, method, *args, **kwargs):
		self.__lock.acquire()
		data = method(self, *args, **kwargs)
		if data is not None:
			self.__peer.address.total += 1
			if data: self.__peer.address.fresh += 1
		self.__lock.release()
		return data

	def __request(self, tag, data):
		if self.__reply.pending and not self.__reply.service(): raise OSError
		socket = self.__peer.socket
		socket.send(tag + data)
		while True:
			code = socket.recv(1)
			if code == unavailable(tag): return False
			if code ==   available(tag): return True
			if not self.__reply.service(code, True): raise OSError
