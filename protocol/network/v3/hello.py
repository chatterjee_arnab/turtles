from connection.address import Address
from connection.peer    import Peer

from database.cache     import cache

from protocol.version   import commodity

from socket             import AF_INET6

def main(address, socket, network):
	socket.send(Peer.my_commodity)
	version = int.from_bytes(socket.recv(2), "big")
	version, deprecated = commodity(version)
	if version and deprecated:
		socket.close()
		return
	socket.send(Peer.my_record_size)
	record_size = int.from_bytes(socket.recv(3), "big")
	if type(address) is bytes: address = Address(address, socket.recv(2))
	else: socket.send((Peer.IPv4_port, Peer.IPv6_port)[socket.family is AF_INET6])
	socket.send(cache.length.to_bytes(4, "big"))
	chain_length = int.from_bytes(socket.recv(4), "big")
	return Peer(address, socket, network, version, record_size, chain_length)
