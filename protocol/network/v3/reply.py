from connection.peer          import Peer

from daemon.networkd          import peerlist

from database.block           import Block
from database.cache           import cache
from database.header          import Header
from database.record          import Record

from daemon.miningd           import append, pool, pooled

from protocol.network.v3      import tag
from protocol.network.v3.tag  import available, unavailable
from protocol.network.v3.sync import synchronize

from protocol.version         import commodity

from socket                   import AF_INET, AF_INET6

from utility.bitops           import put
from utility.hasher           import gethash, hashtype
from utility.merkle           import get_path
from utility.number           import decompress

class Reply:

	def __init__(self, peer): self.__peer, self.__pending = peer, None

	@property
	def pending(self): return self.__pending

	def service(self, code=None, pending=False):
		if self.__pending: return self.__pending(False)
		if not code:
			socket = self.__peer.socket
			if socket.idle(): return True
			code = socket.recv(1)
		if code == tag.record_certified: return self.__record(True)
		if code == tag.block           : return self.__block()
		if code == tag.block_broadcast : return self.__block_broadcast(pending)
		if code == tag.record          : return self.__record(False)
		if code == tag.record_broadcast: return self.__record_broadcast(pending)
		if code == tag.peers           : return self.__peers()
		if code == tag.headers         : return self.__headers()
		if code == tag.synchronize     : return self.__synchronize(pending)

	def __block(self):
		if Peer.lightweight or not self.__peer.record_size: return
		socket = self.__peer.socket
		blockid = socket.recv(32)
		recordids = Block.get_recordids(blockid)
		if not recordids: return socket.send(unavailable(tag.block))
		return socket.send(available(tag.block) + (len(recordids) - 1).to_bytes(3, "big") + b''.join(recordids))

	def __block_broadcast(self, pending):
		socket = self.__peer.socket
		if  self.__pending:
			self.__pending = None
			hdr, header = self.__state
			hlen, previous = bytes((len(hdr) - 1,)), header.previous
		else:
			previous = Header.get(socket.recv(32))
			if not previous: return True
			previous.hashtype = hashtype(previous.previous)
			hlen = socket.recv(1)
			hdr  = socket.recv(hlen[0] + 1)
			if hdr[0] < 128: version = hdr[0]
			else: version = decompress(hdr[0:2], 2)
			version, deprecated = commodity(version)
			if deprecated: return
			header, offset = version.header.deserialize(previous, hdr)
			if not header or offset != len(hdr): return
		if not Peer.lightweight and header.blocksiz:
			if pending:
				self.__pending = self.__block_broadcast
				self.__state = hdr, header
				return True
			block = self.request.block(header.blockid, previous.blockid)
			if not block: return
		else: block = []
		status = append(header, block)
		if status is None: return
		socket.send(available(tag.block_broadcast))
		if not Peer.lightweight:
			if status: self.__peer.broadcast = "block_broadcast", previous.blockid + hlen + hdr
			else     : self.__peer.broadcast = None
		return True

	def __certificate(self, record):
		blockids = Block.get_blockids(recordid)
		if not blockids: return
		for blockid in blockids:
			certificate = Header.get_certificate(blockid)
			if certificate: break
		if not certificate: return
		previous, pownonce, mrkltree = certificate
		hashtype = hashtype(previous)
		leaf = gethash(record + pownonce, hashtype)
		count = Block.get_count(blockid)
		merkle_path = get_path(mrkltree, leaf, count)
		merkle_len = len(merkle_path)
		nonce_len = len(pownonce) - 1
		metadata = bytearray(2)
		put(bytes((hashtype   << 5,)), 0, 3, metadata)
		put(bytes((nonce_len  << 3,)), 3, 5, metadata)
		put(bytes((merkle_len << 3,)), 8, 5, metadata)
		return bytes(metadata) + pownonce + b''.join(merkle_path)

	def __headers(self):
		socket = self.__peer.socket
		blockid = socket.recv(32)
		header = Header.get_next(blockid)
		if not header: return socket.send(unavailable(tag.headers))
		socket.send(available(tag.headers))
		count, headers, size = 0, [], 0
		while header:
			count += 1
			headers.append(commodity(header.protocol)[0].header.serialize(header))
			size += len(headers[-1])
			blockid = header.blockid
			header = Header.get_next(header.blockid)
		return socket.send((count - 1).to_bytes(4, "big") + (size - 1).to_bytes(5, "big") + b''.join(headers))

	def __peers(self):
		socket = self.__peer.socket
		filters, count = socket.recv(2)
		filters, zero = bytearray(bin(filters)[2:].encode()), ord('0')
		for index in range(4)   : filters[index] = (True, False)[filters[index] - zero]
		for index in range(4, 8): filters[index] = (False, True)[filters[index] - zero]
		xIPv4, xIPv6, xfull, xlight, xnewer, xolder, xlower, xidle = filters
		if xIPv4 and xIPv6 or xfull and xlight: return
		if count < 30: count += 1
		else: count = 30
		IPv4, IPv6 = 2 * (15 * [None],)
		nIPv4, nIPv6 = 0, 0
		record_size, version = self.__peer.record_size, self.__peer.commodity.version
		for peer in peerlist(True, self.__peer):
			if xIPv4  and peer.socket.family is AF_INET          : continue
			if xIPv6  and peer.socket.family is AF_INET6         : continue
			if xfull  and peer.record_size                       : continue
			if xlight and not peer.record_size                   : continue
			if xnewer and peer.commodity.version.number > version: continue
			if xolder and peer.commodity.version.number < version: continue
			if xlower and peer.record_size < record_size         : continue
			if xidle  and not peer.address.total                 : continue
			address = peer.str()
			if peer.socket.family is AF_INET:
				IPv4[nIPv4] = address
				nIPv4 += 1
			else:
				IPv4[nIPv4] = address
				nIPv4 += 1
			count -= 1
			if not count: break
		total = nIPv4<<4 | nIPv6
		if not total: return socket.send(unavailable(tag.peers))
		total, IPv4, IPv6 = bytes((total,)), b''.join(IPv4[:nIPv4]), b''.join(IPv6[:nIPv6])
		return socket.send(available(tag.peers) + total + IPv4 + IPv6)

	def __record(self, certified):
		if Peer.lightweight or not (certified ^ self.__peer.record_size): return
		socket = self.__peer.socket
		code, record, recordid = (tag.record, tag.record_certified)[certified], None, socket.recv(32)
		if not certified: record = pooled(recordid)
		if record: record = record.record
		else:
			record = Record.get(recordid)
			if not record: return socket.send(unavailable(code))
		data = available(code) + (len(record) - 1).to_bytes(3, "big") + record
		if not certified: return socket.send(data)
		certificate = self.__certificate(record)
		if not certificate: return socket.send(unavailable(code))
		return socket.send(data + certificate)

	def __record_broadcast(self, pending):
		if Peer.lightweight: return
		socket = self.__peer.socket
		if  self.__pending:
			self.__pending = None
			record = self.__state
		else:
			record = Record()
			record.recordid = socket.recv(32)
			if record.exists or pooled(record.recordid):
				self.__peer.broadcast = None
				return True
			if pending:
				self.__pending = self.__record_broadcast
				self.__state = record
				return True
		record = self.request.record(record.recordid, cache.blockid, True, True)
		if not record or not (record.record[0] or record.record[1]&1): return
		socket.send(available(tag.record_broadcast))
		if pool(record): self.__peer.broadcast = "record_broadcast", record.record
		else: self.__peer.broadcast = None
		return True

	def __synchronize(self, pending):
		peer = self.__peer
		socket = peer.socket
		if self.__pending: self.__pending = None
		else:
			chain_length = int.from_bytes(socket.recv(4), "big")
			if chain_length < peer.chain_length: return
			peer.chain_length, peer.synced = chain_length, cache.length <= peer.chain_length
			socket.send(available(tag.synchronize) + cache.length.to_bytes(4, "big"))
			if pending:
				self.__pending = self.__synchronize
				return True
		return synchronize(self.__peer, self.request)
