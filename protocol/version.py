from database.cache   import cache

from utility.datetime import datetime

def _protocol(protocol, version, commodity=None):
	if version is None:
		if commodity is None: return len(protocol) - 1
		elif commodity: return len(protocol) - 1 << 1
		else: return (len(protocol) << 1) - 1
	index = version
	if commodity is not None: index >>= 1
	if index >= len(protocol) or not protocol[index]: return None, True
	module, expiry = protocol[index]
	deprecated = expiry and (type(expiry) is int and cache.length > expiry or cache.blocktim > expiry)
	return module, deprecated

from protocol.commodity import v0
_commodity = (\
(v0, None),
)
def commodity(version=None): return _protocol(_commodity, version, True)

from protocol.document import v0
_document = (\
(v0, None),
)
def document(version=None): return _protocol(_document, version)

from protocol.transfer import v1
_transfer = (\
(v1, None),
)
def transfer(version=None): return _protocol(_transfer, version, False)

from protocol.network import v0, v1, v2, v3
_network = (\
(v0, 1000),
(v1, 1000),
(v2, datetime(2021, 7, 1)),
(v3, None),
)
def network(version=None): return _protocol(_network, version)
