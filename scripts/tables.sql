create table Users
(	userid   binary(33)        primary key,
	balance   bigint unsigned  not null default 0,
	consumed  bigint unsigned  not null default 0,
	mining   tinyint unsigned  not null default 0
);

create table Header
(	blockid  binary(32)        primary key,
	previous binary(32)        not null,
	protocol smallint unsigned not null,
	mrkltree longblob          not null,
	consumed   bigint unsigned not null,
	mining    tinyint unsigned not null,
	blocksiz      int unsigned not null,
	blocktim datetime          not null,
	hashwind binary(32)        not null,
	pownonce varbinary(32)     not null,
	totalcon   bigint unsigned not null,
	miningid  binary(33),
	length        int unsigned not null,
	longest  boolean           check (longest = true),
	unique  (length, longest)
);

alter table Header add  constraint  protocol check (protocol % 2 = 0);
alter table Header add  constraint  blocktim check (blocktim >= '21-1-1');
alter table Header add  constraint  previous foreign key (previous) references Header (blockid) on delete cascade;
alter table Header add  constraint  miningid foreign key (miningid) references  Users  (userid);

-- alter table Header drop constraint  protocol;
-- alter table Header drop constraint  blocktim;
-- alter table Header drop foreign key previous;
-- alter table Header drop foreign key miningid;

create table Record
(	recordid binary(32) primary key,
	record   mediumblob not null
);

create table Block
(	blockid  binary(32)         not null,
	offset   mediumint unsigned not null,
	recordid binary(32)         not null,
	primary  key (blockid, offset)
);

alter table Block add  constraint   blockid foreign key ( blockid) references Header ( blockid) on delete cascade;
alter table Block add  constraint  recordid foreign key (recordid) references Record (recordid);

-- alter table Block drop foreign key  blockid;
-- alter table Block drop foreign key recordid;

create table Wallet
(	userid   binary(33) primary key,
	enckey   binary(32) not null,
	mining   boolean    not null
);

alter table Wallet add  constraint  userid foreign key (userid) references Users (userid);

-- alter table Wallet drop foreign key userid;

create table Transfer
(	txn      binary(32)   primary key,
	payee    binary(33)   not null,
	amount   int unsigned not null
);

alter table Transfer add  constraint   txn  foreign key ( txn ) references Record (recordid);
alter table Transfer add  constraint  payee foreign key (payee) references Users  ( userid );

-- alter table Transfer drop foreign key  txn ;
-- alter table Transfer drop foreign key payee;

create table Used
(	transfer binary(32) not null,
	usedby   binary(32) not null,
	primary  key (transfer, usedby)
);

alter table Used add  constraint  transfer foreign key (transfer) references Transfer (txn);
alter table Used add  constraint   usedby  foreign key ( usedby ) references Transfer (txn);

-- alter table Used drop foreign key transfer;
-- alter table Used drop foreign key  usedby ;
