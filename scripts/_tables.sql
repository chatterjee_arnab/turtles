create table Users
(	userid   binary(33)        primary key,
	balance   bigint unsigned  not null default 0,
	consumed  bigint unsigned  not null default 0,
	mining   tinyint unsigned  not null default 0
);

create table Header
(	blockid  binary(32)        primary key,
	previous binary(32)        not null,
	protocol smallint unsigned not null,
	mrkltree longblob          not null,
	consumed   bigint unsigned not null,
	mining    tinyint unsigned not null,
	blocksiz      int unsigned not null,
	blocktim datetime          not null,
	hashwind binary(32)        not null,
	pownonce varbinary(32)     not null,
	totalcon   bigint unsigned not null,
	miningid  binary(33),
	length        int unsigned not null,
	longest  boolean           check (longest = true),
	unique  (length, longest)
);

alter table Header add  constraint  protocol check (protocol % 2 = 0);
alter table Header add  constraint  blocktim check (blocktim >= '21-1-1');
alter table Header add  constraint  previous foreign key (previous) references Header (blockid) on delete cascade;
alter table Header add  constraint  miningid foreign key (miningid) references  Users  (userid);

-- alter table Header drop constraint  protocol;
-- alter table Header drop constraint  blocktim;
-- alter table Header drop foreign key previous;
-- alter table Header drop foreign key miningid;

create table Record
(	recordid binary(32) primary key,
	record   mediumblob not null
);

create table Block
(	blockid  binary(32)         not null,
	offset   mediumint unsigned not null,
	recordid binary(32)         not null,
	primary  key (blockid, offset)
);

alter table Block add  constraint   blockid foreign key ( blockid) references Header ( blockid) on delete cascade;
alter table Block add  constraint  recordid foreign key (recordid) references Record (recordid);

-- alter table Block drop foreign key  blockid;
-- alter table Block drop foreign key recordid;

create table Document
(	document binary(32) primary key,
	follower binary(32) unique
);

alter table Document add  constraint  follower foreign key (follower) references Document (document);
alter table Document add  constraint  document foreign key (document) references  Record  (recordid);

-- alter table Document drop foreign key follower;
-- alter table Document drop foreign key document;

create table Wallet
(	userid   binary(33)      primary key,
	enckey   binary(32)      not null,
	mining   boolean         not null
);

alter table Wallet add  constraint  Wallet__userid foreign key (userid) references Users (userid);

-- alter table Wallet drop foreign key Wallet__userid;

create table Folder
(	filehash binary(32)       not null,
	hashtype tinyint unsigned not null,
	document binary(32)       not null,
	primary  key (filehash, hashtype)
);

alter table Folder add  constraint  hashtype check (hashtype between 0 and 7);
alter table Folder add  constraint  Folder__document foreign key (document) references Document (document);

-- alter table Folder drop constraint  hashtype;
-- alter table Folder drop foreign key Folder__document;

create table Usergroup
(	groupid  binary(32)   not null,
	userid   binary(33)   not null,
	length   int unsigned not null,
	primary  key (groupid, userid)
);

alter table Usergroup add  constraint  userid foreign key (userid) references Users (userid);

-- alter table Usergroup drop foreign key userid;

create table Account
(	accnum   binary(32)       not null,
	groupid  binary(32)       not null,
	locking      int unsigned not null,
	plenum   tinyint unsigned not null,
	quorum   tinyint unsigned not null,
	length       int unsigned not null,
	primary  key (accnum, groupid)
);

alter table Account add  constraint  plenum_quorum check (plenum >= quorum);
alter table Account add  constraint  groupid foreign key (groupid) references Usergroup (groupid);

-- alter table Account drop constraint  plenum_quorum;
-- alter table Account drop foreign key groupid;

create table Code
(	axscode  binary(32)   primary key,
	csegment blob         not null,
	length   int unsigned not null
);

create table Data
(	axsdata  binary(32)   primary key,
	dsegment blob         not null,
	metadata tinyint      not null,
	length   int unsigned not null
);

alter table Data add  constraint  metadata check (metadata between 1 and 8);

-- alter table Data drop constraint  metadata;

create table Formula
(	axsform  binary(32)   primary key,
	axscode  binary(32)   not null,
	axsdata  binary(32)   not null,
	length   int unsigned not null
);

alter table Formula add  constraint  axscode foreign key (axscode) references Code (axscode);
alter table Formula add  constraint  axsdata foreign key (axsdata) references Data (axsdata);

-- alter table Formula drop foreign key axscode;
-- alter table Formula drop foreign key axsdata;

create table Transfer
(	txn      binary(32)       not null,
	oicindex tinyint unsigned not null,
	oictype  tinyint unsigned not null,
	amount       int unsigned not null,
	accnum   binary(32)       not null,
	mapping  tinyblob         not null,
	primary  key (txn, oicindex, oictype)
);

alter table Transfer add  constraint  Transfer__accnum foreign key (accnum) references Account (accnum);
alter table Transfer add  constraint  txn foreign key (txn) references  Record  (recordid);

-- alter table Transfer drop foreign key Transfer__accnum ;
-- alter table Transfer drop foreign key txn;

create table STXO
(	txn      binary(32)       not null,
	oicindex tinyint unsigned not null,
	oictype  tinyint unsigned not null,
	usedby   binary(32)       not null,
	primary  key (txn, oicindex, oictype, usedby)
);

alter table STXO add  constraint  STXO__txn_oicindex_oictype foreign key(txn, oicindex, oictype) references Transfer (txn, oicindex, oictype);
alter table STXO add  constraint  usedby foreign key (usedby) references Record (recordid);

-- alter table STXO drop foreign key STXO__txn_oicindex_oictype;
-- alter table STXO drop foreign key usedby;

create table UTXO
(	txn      binary(32)       not null,
	oicindex tinyint unsigned not null,
	oictype  tinyint unsigned not null,
	primary  key (txn, oicindex, oictype)
);

alter table UTXO add  constraint  UTXO__txn_oicindex_oictype foreign key(txn, oicindex, oictype) references Transfer (txn, oicindex, oictype);

-- alter table UTXO drop foreign key UTXO__txn_oicindex_oictype;

create table Owned
(	accnum   binary(32)      primary key,
	balance  bigint unsigned not null
);

alter table Owned  add  constraint   Owned__accnum foreign key (accnum) references Account (accnum);

-- alter table Owned  drop foreign key  Owned__accnum;

create table Shared
(	accnum   binary(32)      primary key,
	funds    bigint unsigned not null
);

alter table Shared add  constraint  Shared__accnum foreign key (accnum) references Account (accnum);

-- alter table Shared drop foreign key Shared__accnum;
