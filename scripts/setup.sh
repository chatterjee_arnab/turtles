#!/usr/bin/env bash

PS4='$BASH_SOURCE@$LINENO: '
set -ex

wget -qOmysql.deb https://repo.mysql.com/mysql-apt-config_0.8.16-1_all.deb
dpkg -i mysql.deb
rm   -f mysql.deb
apt  install --upgrade mysql-server python3.9 pip3 rustc
pip3 install --upgrade pip
pip3 install --upgrade ecdsa mysql-connector pyaes pygost pep517 blake3

#pip3 uninstall pep517
#apt  purge rustc
#apt  autoremove --purge

cd "$(dirname "$BASH_SOURCE")"
echo -e "\nalias turtles='$(pwd)/../turtles.py'\n" >>~/.bash_aliases

mysql -v <database.sql
