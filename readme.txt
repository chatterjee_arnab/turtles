Replace 'username' with your username (within single quotes) in scripts/database.sql
Enter the same in 'credentials' dictionary's user key-value in database/database.py
Install dependencies and create local database by running: sudo scripts/setup.sh
On successful installation, run crypto-commodity application using turtles command.
