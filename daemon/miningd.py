from database                    import database
from database.block              import Block
from database.cache              import cache
from database.header             import Header
from database.record             import Record
from database.used               import Used
from database.users              import Users
from database.transfer           import Transfer
from database.wallet             import Wallet

from protocol.commodity.v        import version
from protocol.commodity.v.header import adjust_window, proof_of_work, set_blockid, set_blocktim, verify_time
from protocol.commodity.v.main   import marshal, next_idealsiz
from protocol.transfer.v.main    import serialize, verify

from threading                   import Lock, Thread

from utility                     import lock
from utility.datetime            import datetime
from utility.exit                import status
from utility.hasher              import gethash, hashtype
from utility.merkle              import augmented_tree
from utility.security            import authenticate, generate, password

_broadcast, _capacity, _lock, _size = 4 * (None,)
_block, _local, _mempool = [], [], []

def _append(header, base):
	new = None
	while not new:
		if   len( _local ): new =  _local .pop(0)
		elif len(_mempool): new = _mempool.pop(0)
		else: return
		if not new.transfer.verified(header.previous.blockid): new = None
	for index in range(1, len(_block)):
		if	_block[index].transfer.txn == new.transfer.txn:
			header.blocksiz += len(new.record) - len(_block[index].record)
			_block[index] = new
			new = None
			break
	if new and header.blocksiz + len(new.record) <= base.transfer.amount:
		_block.append(new)
		base.count += 1
		header.blocksiz += len(new.record)

def _base(header, base, key):
	base.record, base.pownonce = b'', bytes(1)
	base.transfer.payee, base.transfer.amount = None, 0
	ideal_size = cache.invariant // header.hashwind
	if ideal_size <= 3:
		base.transfer.txn = None
		return
	lock.acquire()
	if header.previous.blockid != cache.blockid:
		lock.release()
		return
	max_quality, miningid = 0, None
	for userid in Wallet.get_userids():
		consumption, mining = Users.get_consumed(userid), Users.get_mining(userid)
		quality = consumption / (1<<mining)
		if quality > max_quality:
			max_quality, miningid = quality, userid
			header.consumed, header.mining = consumption, mining
	lock.release()
	if miningid: adjust_window(header)
	elif not header.previous.totalcon: miningid = generate(key, True)
	else: return
	_block.append(base)
	base.count, base.transfer.txn = 1, miningid
	if   ideal_size < 73: base.pownonce = bytes( 2)
	elif ideal_size < 95: base.pownonce = bytes(18)
	else:
		base.pownonce, base.transfer.payee = bytes(3 * ((ideal_size > 125) + 1)), generate(key, True)
		_maturity(header, base, key)

def _init(header, base, key):
	lock.acquire()
	header.previous, header.hashwind, header.hashtype = Header.get(cache.blockid), cache.hashwind, hashtype(cache.blockid)
	lock.release()
	global _block
	if  _block:
		_block.pop(0)
		for record in _block:
			if record.transfer.verified(header.previous.blockid): _mempool.append(record)
		_block = []
	header.consumed, header.mining, header.blocksiz = 3 * (0,)
	_base(header, base, key)

def _maturity(header, base, key):
	lock.acquire()
	if header.previous.blockid != cache.blockid:
		lock.release()
		return
	index, record = -1, Record()
	for userid in Wallet.get_all_mining():
		if Users.get_consumed(userid): continue
		for transfer in Transfer.get_by_payee(userid):
			if transfer.amount > 130 and verify(transfer, header.previous.blockid):
				record.transfer = transfer
				break
		if record.transfer: break
	lock.release()
	if not record.transfer: return
	_block.append(record)
	base.count += 1
	userid = transfer.payee
	transfer.payee, transfer.verifier = base.transfer.payee, verify
	serialize(record, userid, key)
	header.blocksiz += len(record.record)
	_lock.release()
	global _broadcast
	while True:
		_lock.acquire()
		if not _broadcast: break
		_lock.release()
	_broadcast = "record_broadcast", record.record
	return True

def _redo(blockid):
	header, user = Header(), Users()
	header.blockid, header.longest = blockid, True
	header.set_longest()
	miningid = Header.get_miningid(blockid)
	if not miningid: return True
	consumed, user.consumed = Header.get_consumed(blockid), Users.get_consumed(miningid)
	mining, user.mining = Header.get_mining(blockid), Users.get_mining(miningid)
	if consumed != user.consumed or mining != user.mining:
		database.rollback()
		header.delete()
		return
	user.userid, user.mining = miningid, 1
	user.set_mining()
	for recordid in Block.get_recordids(blockid):
		consumption, transfer = Record.get_size(recordid), Transfer.get(recordid)
		amount = transfer.amount
		user.userid, user.balance = transfer.payee, amount
		user.set_balance()
		txn = Used.get_by_usedby(recordid)
		if txn:
			transfer = Transfer.get(txn)
			user.userid, user.balance = transfer.payee, -(amount + consumption)
			user.set_balance()
		else: user.userid = miningid
		user.consumed = consumption
		user.set_consumed()
	return True

def _undo(blockid):
	header, user = Header(), Users()
	header.blockid, header.longest = blockid, False
	header.set_longest()
	miningid = Header.get_miningid(blockid)
	if not miningid: return
	user.userid, user.mining = miningid, -1
	user.set_mining()
	for recordid in Block.get_recordids(blockid):
		consumption, transfer = Record.get_size(recordid), Transfer.get(recordid)
		amount = transfer.amount
		user.userid, user.balance = transfer.payee, -amount
		user.set_balance()
		txn = Used.get_by_usedby(recordid)
		if txn:
			transfer = Transfer.get(txn)
			user.userid, user.balance = transfer.payee, amount + consumption
			user.set_balance()
		else: user.userid = miningid
		user.consumed = -consumption
		user.set_consumed()

def _validate(header, block):
	base, blocksiz = not block, 0
	for record in block:
		blocksiz += len(record.record)
		if record.count is not None:
			if record.record[1] != header.protocol: return
			if record.count != len(block): return
			if record.pownonce != header.pownonce: return
			if record.transfer.amount != next_idealsiz(header): return
			header.miningid, base = record.transfer.txn, True
	if header.blocksiz != blocksiz or not base: return
	mrkltree = augmented_tree(block, header.pownonce, header.hashtype)
	if header.mrkltree[-1] != mrkltree[-1]: return
	header.mrkltree = mrkltree
	return True

def append(header, block, local=False):
	if not local:
		header.miningid = None
		if block and not _validate(header, block): return
	b, used, user = Block(), Used(), Users()
	header.previous, b.blockid = header.previous.blockid, header.blockid
	lock.acquire()
	if not header.put():
		lock.release()
		return False
	for offset, record in enumerate(block):
		if record.put():
			transfer = record.transfer
			txn = transfer.txn
			if    transfer.payee: user.userid = transfer.payee
			else: transfer.payee, user.userid = 2 * (txn,)
			user.put()
			transfer.txn = record.recordid
			transfer.amount -= len(record.record)
			transfer.put()
			if not record.count:
				used.transfer, used.usedby = txn, record.recordid
				used.put()
		b.offset, b.recordid = offset, record.recordid
		b.put()
	if header.length <= cache.length:
		lock.release()
		return False
	chain, redo, undo = [header.blockid], header.previous, cache.blockid
	while redo != undo:
		_undo(undo)
		chain.insert(0, redo)
		redo, undo = Header.get_previous(redo), Header.get_previous(undo)
	for blockid in chain:
		if not _redo(blockid):
			lock.release()
			return
	cache.blockid  = header.blockid
	cache.blocktim = header.blocktim
	cache.hashwind = header.hashwind
	cache.totalcon = header.totalcon
	cache.length  += 1
	lock.release()
	return True

def local():
	global _broadcast
	broadcast = _broadcast
	if broadcast: _broadcast = None
	return broadcast

def main(mempool_size, min_fee):
	userids = Wallet.get_all_mining()
	if not userids: key = password("Enter new mining password: ")
	else: key = authenticate(userids[0], "Enter mining password: ")
	if not key: return
	status("turtles", "mining", False)
	global _capacity, _lock, _size
	_capacity, _lock, _size = mempool_size, Lock(), 0
	Thread(daemon=True, target=run, args=(key, min_fee)).start()

def pool(record, local=False):
	if not record: return
	if local:
		global _broadcast
		if _lock:
			while True:
				_lock.acquire()
				if not _broadcast: break
				_lock.release()
		else:
			while _broadcast: pass
		_broadcast = "record_broadcast", record.record
		if _lock: _lock.release()
	if not _lock or pooled(record.recordid): return
	global _size
	_lock.acquire()
	if local: _local.append(record)
	else:
		size = len(record.record)
		if  _size +  size <= _capacity:
			_mempool.append(record)
			_size += size
	_lock.release()
	return True

def pooled(recordid):
	if not _lock: return
	record = Record()
	record.recordid = recordid
	_lock.acquire()
	try: record = _block[_block.index(record)]
	except ValueError:
		try: record = _local[_local.index(record)]
		except ValueError:
			try: record = _mempool[_mempool.index(record)]
			except ValueError: record = None
	_lock.release()
	return record

def run(key, min_fee):
	header, base = Header(), Record()
	header.previous, header.protocol, base.transfer = Header(), version.number, Transfer()
	global _broadcast, _block
	while True:
		_lock.acquire()
		if header.previous.blockid != cache.blockid: _init(header, base, key)
		elif _block: _append(header, base)
		_lock.release()
		if header.previous.blocksiz and not _block: continue
		while header.previous.blockid == cache.blockid:
			header.pownonce = base.pownonce
			if _block:
				blocksiz = header.blocksiz
				marshal(header, base)
				if header.blocksiz != blocksiz: size_factor = set_blocktim(header)
			else: size_factor = set_blocktim(header)
			header.mrkltree = augmented_tree(_block, base.pownonce, header.hashtype)
			hdr = set_blockid(header)
			if proof_of_work(header, size_factor):
				header.miningid = base.transfer.txn
				if append(header, _block, True):
					while True:
						_lock.acquire()
						if not _broadcast and verify_time(header.blocktim): break
						_lock.release()
					_broadcast = "block_broadcast", header.previous + bytes((len(hdr) - 1,)) + hdr
					_lock.release()
				_lock.acquire()
				_block, header.previous = [], Header()
				_lock.release()
				break
			nonce_size = len(base.pownonce)
			pownonce = int.from_bytes(base.pownonce, "big") + 1
			nonce_size = max(nonce_size, (pownonce.bit_length()-1 >> 3) + 1)
			base.pownonce = pownonce.to_bytes(nonce_size, "big")
			if verify_time(header.blocktim): break
