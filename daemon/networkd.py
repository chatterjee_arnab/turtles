from connection.address  import Address
from connection.nodebase import seeds
from connection.peer     import Peer
from connection.peerlist import addresses
from connection.socket   import socket

from daemon.miningd      import local

from datetime            import timedelta

from ipaddress           import ip_address

from socket              import AF_INET, AF_INET6, inet_pton

from threading           import Lock, Thread

from utility.datetime    import datetime

_max_peers, _peer_count = 2 * (0,)
_network, _peers = 2 * (None,)
_lock, _request = Lock(), Lock()

def _client(IPv4_port, IPv6_port, addresses):
	if   IPv4_port is 0: exclude = AF_INET
	elif IPv6_port is 0: exclude = AF_INET6
	else               : exclude = None
	while True:
		for address in addresses:
			host = ip_address(address.host)
			if (host.is_loopback or host.is_private) and (address.port == IPv4_port or address.port == IPv6_port): continue
			if address.family == exclude: continue
			peer = Peer(address, *(5 * (None,)))
			_lock.acquire()
			if peer in _peers: peer = None
			_lock.release()
			if not peer: continue
			while _peer_count == _max_peers: pass
			Thread(target=_init, args=(address,)).start()
		timeout = datetime.utcnow() + timedelta(minutes=_peer_count)
		while datetime.utcnow() < timeout: pass
		addresses = request("peers", exclude)
		if    addresses : addresses.sort(key=Address.quality, reverse=True)
		else: addresses = ()

def _init(address, socket=None):
	try:
		if not socket:
			socket = address.connect()
			if not socket: return
		socket.send(Peer.my_network)
		version = int.from_bytes(socket.recv(2), "big")
		version, deprecated = _network(version)
		if version and deprecated:
			socket.close()
			return
		peer = version.hello.main(address, socket, version)
	except OSError:
		if socket: socket.close()
		return
	global _peer_count
	unlisted = True
	while unlisted:
		_lock.acquire()
		if _peer_count != _max_peers:
			if peer in _peers:
				socket.close()
				_lock.release()
				return
			_peers[_peer_count] = peer
			_peer_count += 1
			unlisted = False
		_lock.release()
	try: version.run.main(peer)
	except (IndexError, OSError): pass
	socket.close()
	_lock.acquire()
	peer.chain_length = 0
	index = _peers.index(peer)
	_peer_count -= 1
	_peers[index] = _peers[_peer_count]
	_lock.release()

def _server(port, family):
	server = socket.create_server(port, family, _max_peers)
	while True:
		connection, address = server.accept()
		Thread(target=_init, args=(inet_pton(family, address[0]), connection)).start()

def broadcast():
	local_broadcast = None
	while True:
		broadcast, peers = local(), peerlist(False)
		if not broadcast: broadcast = local_broadcast
		if not peers:
			local_broadcast = broadcast
			continue
		if broadcast:
			for peer in peers:
				if peer.synced:
					peer.request = broadcast
					local_broadcast = None
		for sender in peers:
			broadcast = sender.broadcast
			if not broadcast: continue
			for peer in peers:
				if peer is not sender and peer.synced: peer.request = broadcast

def main(IPv4_port, IPv6_port, max_peers, record_size, mempool_size):
	from protocol.version import network
	global _max_peers, _network, _peers
	_max_peers, _network, _peers = max_peers, network, max_peers * [None]
	Peer.init(IPv4_port, IPv6_port, record_size)
	if IPv4_port: Thread(target=_server, args=(IPv4_port, AF_INET )).start()
	if IPv6_port: Thread(target=_server, args=(IPv6_port, AF_INET6)).start()
	for address in seeds:
		try:
			socket = address.connect()
			if not socket: continue
			count = socket.recv(1)
			IPv4_count, IPv6_count = count[0] >> 4, count[0] & 15
			for i in range(IPv4_count):
				address = socket.recv(6)
				addresses.append(Address(address[: 4], address[ 4:]))
			for i in range(IPv6_count):
				address = socket.recv(18)
				addresses.append(Address(address[:16], address[16:]))
		except OSError: pass
		if socket: socket.close()
	Thread(target=_client, args=(IPv4_port, IPv6_port, addresses)).start()
	broadcast()

def peerlist(sort=False, exclude=None):
	_lock.acquire()
	peers = _peers[:_peer_count]
	_lock.release()
	try:
		if exclude: peers.remove(exclude)
	except ValueError: pass
	if sort: peers.sort(key=Peer.quality, reverse=True)
	return peers

def request(identifier, data):
	_request.acquire()
	peers = peerlist(True)
	peer_count = len(peers)
	tries = (peer_count >> 1) + 1
	for peer in peers: peer.request = identifier, data
	reply = False
	while peer_count and tries:
		index = 0
		while index != peer_count:
			peer = peers[index]
			if peer.chain_length:
				request, reply = peer.reply
				if request == (identifier, data):
					if reply:
						tries = 0
						break
					else: tries -= 1
				else:
					reply = False
					if peer.request == (identifier, data):
						index += 1
						continue
			peers[index] = peers[-1]
			peers.pop()
			peer_count -= 1
	_request.release()
	return reply
