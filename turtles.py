#!/usr/bin/python3.9 -BWignore

from daemon           import miningd, networkd

from database         import database

from messages.turtles import prompt, turtles

from service.init     import init

from sys              import argv

from threading        import Thread

from utility          import animation, lock
from utility.exit     import status

def shell(IPv4_port, IPv6_port, max_peers=0, record_size=0, mempool_size=0, min_fee=0):
	database.connect()
	if IPv4_port or IPv6_port:
		lock.create()
		if mempool_size: miningd.main(mempool_size, min_fee)
		Thread(daemon=True, target=networkd.main, args=(IPv4_port, IPv6_port, max_peers, record_size, mempool_size)).start()
	command_status = None
	while True:
		if command_status: command_status = str(command_status) + " "
		else: command_status = " "
		args = input(prompt + command_status).strip().split()
		args[0] = args[0].lower()
		if args[0] == "clear":
			if len(args) is 1:
				print("\33c", end="")
				command_status = 0
			else: command_status = status("turtles", "bad_command_invocation")
		elif args[0] == "exit":
			if len(args) is 1: break
			command_status = status("turtles", "bad_command_invocation")
		else: command_status = init(args, True)
	animator = animation.start()
	lock.acquire()
	database.disconnect()
	animation.stop(animator)

if __name__ == "__main__":
	argc = len(argv)
	if argc is 1: exit(status("turtles", "bad_syntax"))
	if argv[1].isdigit():
		if argc < 3 or not argv[2].isdigit(): exit(status("turtles", "bad_syntax"))
		IPv4_port, IPv6_port = int(argv[1]), int(argv[2])
		if IPv4_port or IPv6_port:
			if IPv4_port >= 1<<16 or IPv6_port >= 1<<16 or argc < 5 or not argv[3].isdigit() or not argv[4].isdigit(): exit(status("turtles", "bad_syntax"))
			max_peers, record_size = int(argv[3]), int(argv[4])
			if record_size:
				if argc < 6 or not argv[5].isdigit(): exit(status("turtles", "bad_syntax"))
				mempool_size = int(argv[5])
				if mempool_size:
					if argc is 7 and argv[6].isdigit(): min_fee = int(argv[6])
					elif argc is 6: min_fee = 0
					else: exit(status("turtles", "bad_syntax"))
				elif argc is 6: min_fee = 0
				else: exit(status("turtles", "bad_syntax"))
				shell(IPv4_port, IPv6_port, max_peers, record_size, mempool_size, min_fee)
			elif argc is 5: shell(IPv4_port, IPv6_port, max_peers, record_size)
			else: exit(status("turtles", "bad_syntax"))
		elif argc is 3: shell(IPv4_port, IPv6_port)
		else: exit(status("turtles", "bad_syntax"))
		exit(status("turtles", "clean_exit", prefix=False))
	argv[1] = argv[1].lower()
	if argv[1] == "version":
		if len(argv) is 2: print(turtles)
		elif len(argv) is 3:
			from protocol import version
			protocol = getattr(version, argv[2].lower(), None)
			if protocol: print(argv[2].capitalize(), "version", protocol())
			else: exit(status("turtles", "bad_syntax"))
		else: exit(status("turtles", "bad_syntax"))
	elif argv[1] == "help": exit(init(argv))
	else: exit(init(argv[1:]))
