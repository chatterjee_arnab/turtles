from select       import select

from socket       import AF_INET, AF_INET6, MSG_WAITALL, SHUT_RDWR, socket

from utility.exit import status

class socket(socket):

	@staticmethod
	def create_connection(address):
		connection = socket((AF_INET, AF_INET6)[':' in address[0]])
		try: connection.connect(address)
		except OSError: return
		return connection

	@staticmethod
	def create_server(port, family, backlog=None):
		server = socket(family)
		try: server.bind(("", port))
		except OSError: exit(status("turtles", "listening_port_in_use"))
		server.listen(backlog)
		return server

	def accept(self):
		sock, address = super().accept()
		connection = socket(fileno = sock.fileno())
		connection.__socket = sock
		return connection, address

	def close(self):
		try:
			self.shutdown(SHUT_RDWR)
			super().close()
		except OSError: pass

	def idle(self): return (True, False)[len(select((self,), (), (), 0)[0])]

	def recv(self, bufsize, flags=0):
		data = super().recv(bufsize, flags | MSG_WAITALL)
		if len(data) != bufsize: raise OSError
		return data

	def send(self, bytes, flags=0):
		self.sendall(bytes, flags)
		return True
