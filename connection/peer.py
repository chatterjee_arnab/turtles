from database.cache     import cache

from datetime           import timedelta

from threading          import Lock

from utility.comparable import Comparable
from utility.datetime   import datetime

class Peer(Comparable):

	@staticmethod
	def init(IPv4_port, IPv6_port, record_size):
		from protocol.version import commodity, network
		Peer.IPv4_port      = IPv4_port  .to_bytes(2, "big")
		Peer.IPv6_port      = IPv6_port  .to_bytes(2, "big")
		Peer.my_record_size = record_size.to_bytes(3, "big")
		Peer.my_network     = network()  .to_bytes(2, "big")
		Peer.my_commodity   = commodity().to_bytes(2, "big")
		Peer.record_max     = record_size
		Peer.lightweight    = record_size is 0

	def __eq__(self, other):
		eq = super().__eq__(other, Peer)
		if eq is not None: return eq
		return self.__address == other.__address

	def __init__(self, address, socket, network, commodity, record_size, chain_length):
		self.__address      = address
		self.__socket       = socket
		self.__network      = network
		self.__commodity    = commodity
		self.__record_size  = record_size
		self.__chain_length = chain_length
		self.__synced       = False
		self.__lock         = Lock()
		self.__reply        = 2 * (None,)
		self.__broadcast, self.__request = 2 * (None,)
		address.fresh   , address.total  = 2 * (0,)

	@property
	def address(self): return self.__address

	@property
	def socket(self): return self.__socket

	@property
	def network(self): return self.__network

	@property
	def commodity(self): return self.__commodity

	@property
	def record_size(self): return self.__record_size

	@property
	def chain_length(self): return self.__chain_length
	@chain_length.setter
	def chain_length(self, chain_length): self.__chain_length = chain_length

	@property
	def broadcast(self):
		broadcast   = self.__broadcast
		if broadcast: self.__broadcast = None
		return broadcast
	@broadcast.setter
	def broadcast(self, broadcast):
		self.__address.total += 1
		if not broadcast: return
		self.__address.fresh += 1
		while self.__broadcast: pass
		self.__broadcast = broadcast

	@property
	def request(self): return self.__request
	@request.setter
	def request(self, request):
		self.__lock.acquire()
		while self.__request:
			if not self.__chain_length:
				self.__lock.release()
				return
		self.__request = request
		self.__lock.release()

	@property
	def reply(self): return self.__reply
	@reply.setter
	def reply(self, reply):
		self.__reply = self.__request, reply
		self.__address.total += 1
		if reply: self.__address.fresh += 1
		self.__request = None

	@property
	def synced(self): return self.__synced
	@synced.setter
	def synced(self, synced):
		if self.__synced: return
		self.__synced = synced

	def quality(self):
		if not self: return -2
		return self.__address.quality()

	def str(self): return self.__address.str()
