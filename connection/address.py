from connection.socket  import socket

from socket             import AF_INET, AF_INET6, inet_pton

from utility.comparable import Comparable

class Address(Comparable):

	def __eq__(self, other):
		eq = super().__eq__(other, Address)
		if eq is not None: return eq
		return self.__host == other.__host and self.__port == other.__port

	def __init__(self, host, port, fresh=0, total=0):
		self.__host , self.__port  = host , port
		self.__fresh, self.__total = fresh, total

	@property
	def host(self): return self.__host

	@property
	def port(self): return self.__port

	@property
	def fresh(self): return self.__fresh
	@fresh.setter
	def fresh(self, fresh): self.__fresh = fresh

	@property
	def total(self): return self.__total
	@total.setter
	def total(self, total): self.__total = total

	@property
	def family(self):
		if type(self.__host) is str: family = self.__host.find(".") is -1
		else: family = len(self.__host) is 16
		return (AF_INET, AF_INET6)[family]

	def connect(self):
		connection = socket.create_connection((self.__host, self.__port))
		if not connection: return
		self.__host = inet_pton(connection.family, self.__host)
		self.__port = self.__port.to_bytes(2, "big")
		return connection

	def quality(self):
		if self.__total is 0: return -1
		return self.__fresh / self.__total

	def str(self): return self.__host + self.__port + self.__fresh.to_bytes(4, "big") + self.__total.to_bytes(4, "big")
