turtles:₹ record recordid.b32[@index.hex]/blockid.b32@index.hex/height.b32@index.hex

Shows contents of the record specified by recordid or blockid@index or height@index.
For recordid, the optional argument is a consumption index for the given recordid,
and if specified, then only the contents of that consumption index are displayed.
Binary data is dumped on the console, or in an empty writeable file (if specified).

Error codes
60: Record general error
61: Record does not exist
62: Block does not exist
63: Block height too large
64: Cindex is not valid
65: Cindex out of range

