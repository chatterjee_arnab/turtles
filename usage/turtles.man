$ turtles IPv4_port IPv6_port [max_peers record_size [mempool_size [min_fee]]]
All arguments must be non-negative integers in usual decimal(base10) representation.
IPv4_port   : Internet Protocol version 4 TCP listening port number (at most 65535).
IPv6_port   : Internet Protocol version 6 TCP listening port number (at most 65535).
Turtles process runs in offline mode if and only if both IPv4 and IPv6 ports are 0.
Next two arguments are given if and only if either IPv4_port or IPv6_port is not 0.
max_peers   : Maximum number of peer nodes to connect (includes both IPv4 and IPv6).
              Node addresses listed in 'connection/peerlist.py' are connected first.
              If the number of successful connections is less than max_peers, then
              addresses are fetched from servers listed in 'connection/nodebase.py'.
              0 indicates an unbounded number of peer nodes (not recommended).
record_size : Maximum record size (in bytes) that is processed and relayed to peers.
              0 indicates that process runs as light-weight node (partial database).
mempool_size: Maximum buffer memory (in bytes) used for unconfirmed valid records.
              0 indicates that the mining daemon thread is not to be started.
              This argument must be provided if and only if record_size is not 0.
min_fee     : Maximum number of record bytes which provide a miner fee of 1 byte.
              For example if min_fee is f, then a record of size R bytes is
              processed, relayed to peer nodes, and possibly included in a locally
              mined block if and only if alleast R/f bytes is given as miner's fee.
              If it is either 0 or omitted, it indicates mining fee is not required.
              This argument may be provided only if mempool_size is not 0.

$ turtles command [options] [empty.file]
command     : Command to be run without starting the turtles shell or daemon thread.
options     : Options to command (type 'turtles command help' for available options)
empty.file  : Blank file for dumping command output in unformatted raw binary form.
              This argument is not applicable for 'usage', 'clear', 'exit' commands.
              To dump output in tabular format, use regular shell redirection (>>).

$ turtles [command] help
command     : Turtles shell command. Omit this to get a list of available commands.
For detailed description of a command's usage, type 'turtles usage command' instead.

$ turtles version [commodity/document/network/transfer]
Shows version number of the given crypto-commodity protocol implemented by turtles.
If no protocol is specified, it shows version number of installed turtles software.

Error codes
110: Database general error
111: Database access denied
112: Database does not exist
113: Database no connection
114: Database read failed
115: Database write failed
116: Database commit failed
120: Turtles general error
121: Bad local system time
122: Network address bad format
123: Listening port in use
124: Empty file unwriteable
125: File size is not zero
126: Bad command invocation
127: Command does not exist
