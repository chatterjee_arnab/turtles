turtles:₹ account [accnum.b32[accnum2.b32]/account.hex/userid.b32]
accnum : Account number (32 bytes hash of account) in base32 (T/t) representation.
accnum2: Simulates the result of merging accnum with accnum2 (both must exist).
account: Binary account specification data encoded in hexadecimal representation.
userid : User ID (compressed public key) in base32 (describes its default account).
Provides a detailed description of specified account, or simulates account creation.

Error codes
10: Account general error
11: Account not parseable
12: AccNum does not exist
13: UserID does not exist

