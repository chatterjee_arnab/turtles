turtles:₹ wallet [-] [userid.b32/accnum.b32] [encryptedprivatekey.b32] [wallet.file]
Access crypto-commodity wallet, set of personal userIDs with encrypted private keys.
If no argument is specified, then a new user ID can be created. If declined by user,
then a complete list of personal user IDs their encrypted private keys is displayed.
The optional - at the start indicates removal of userID(s) from wallet.
userid             : Shows the list of owned and shared accounts having this userID.
accnum             : Shows the list of unused transfer IDs credited to this account.
encryptedprivatekey: Adds a new userID to local wallet (after password is verified).
wallet.file        : Path to a wallet file. If this file is empty, then it must have
                     write permission, and the local wallet database is imported to
                     file after securing it through encryption with wallet password.
                     If this file is non-empty, then it must have read permission,
                     and must be an encrypted file exported from compatible wallet.
                     It is decrypted using password and contents imported to wallet.

Error codes
100: Wallet general error
101: UserID not in wallet
102: AccNum does not exist
103: Non-empty file unreadable
104: Empty file unwriteable
105: Wallet file bad format
106: Password criteria fail
107: Passwords do not match
108: Password is incorrect

