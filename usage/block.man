turtles:₹ block [blockid.b32/height]
Shows contents of the block specified by blockid (in base32) or height (in decimal).
If height is given, then the corresponding block in the longest chain is considered.
If no argument is specified, it shows longest blockchain length and blockchain time.

Error codes
20: Block general error
21: Block does not exist
22: Block height too big

