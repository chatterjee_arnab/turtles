turtles:₹ folder [name.file hash_type] [documentid.b32 [word_offset [word_count]]]
This syntax is used to access the contents of the existing document folder database.
name.file     : If an empty file is specified, then it must have write permission.
                If documentid is not specified, then unformatted contents of the
                document folder from blockchain database are exported to this file.
                If a non-empty file is specified, then it must have read permission.
                It verifies whether the file or its hash are recorded in blockchain.
                If found in blockchain, the corresponding block timestamp is shown.
hash_type     : Numeric code specifying a 32 byte hash algorithm. Possible codes are
                0: No hash (Not recommended)
                1: GOST    (Based on Magma block cipher. See RFC 7801 and RFC 8891.)
                2: Streebog(GOST R34.11-2012. See RFC 6986 or ISO/IEC 10118-3:2018.)
                3: SHA-2   (Secure Hash Algorithm 2. See FIPS PUB 180-4 for detail.)
                4: SHA-3   (Secure Hash Algorithm 3 (Kekkak). See FIPS PUB 202.)
                5: BLAKE2b (Based on ChaCha and HAIFA construction. See RFC 7693.)
                6: BLAKE2s (Based on ChaCha and HAIFA construction. See RFC 7693.)
                7: BLAKE3  (Based on Bao and BLAKE2. Uses a Merkle tree structure.)
                If name.file is empty and hash_type is not 0, then a hash of the
                unformatted contents of the document folder that is exported to file
                is also appended at the end of the same file. This is used to verify
                the integrity of this file when it is imported in some other system.
                Otherwise if name.file is not empty, then its contents are hashed
                using hash_type and this hash is searched in the document folder.
documentid    : If name.file is omitted, then it displays details of this document,
                including information on its ancestor and descendant links (if any).
                If name.file is empty, then contents of that blockchain document
                followed by its successors (if any) are dumped to the specified file
                (document soft delete flags, if any, are obeyed by this operation).
                word_offset and word_count (if given) apply to all successor links.
                If name.file is not empty, searches file hash only in this document.
word_offset   : A non-negative integer that may be specified only with documentid.
                If name.file is omitted, it shows document content from word_offset.
                If name.file is empty, contents from word_offset are dumped to file.
                If name.file is not empty, file hash search starts from word_offset.
word_count    : A positive integer that may be specified only with word_offset.
                If name.file is omitted, it shows document content upto word_count.
                If name.file is empty, contents upto word_count are dumped to file.
                If name.file is not empty, file hash search limited to word_count.
                word_count more than actual count from word_offset denotes till end.

turtles:₹ folder [[+]name.file ...] hash_type [list.file] [[-]previous_docid.b32] [next_userid.b32] [consumptn.file]
In online mode, this syntax is used to add new files to blockchain document folder.
In offline mode, it does a non-persistent simulation of the same functionality.
name.file     : Path to a non-empty file. If the optional + is omitted then its hash
                is to be recorded in the blockchain. + indicates folder import file.
hash_type     : Numeric code specifying a 32 byte hash algorithm. Same as before.
list.file     : Path to a text file containing a list of paths to non-empty files.
                If name.file sequence is omitted, then this argument is mandatory.
previous_docid: Previous document link. An optional - indicates soft delete of the
                nearest non-empty ancestor document that has not been soft deleted.
next_userid   : Signing userID for successor. Successor flag is disabled if omitted.
consumptn.file: Path to a text file containing a list of consumption inputs.
                Format of this file is same as the input format for transfer command
                (if omitted or for shortage, an internal algorithm selects inputs).

Error codes
30: Folder general error
31: File does not exist
32: Non-empty file unreadable
33: Hash type is not valid
34: Import file corruption
35: DocumentID not found
36: Word offset is not valid
37: Word offset out of range
38: Word count  is not valid
39: List file bad format
40: Successor not allowed
41: UserID is not valid
42: Consumption file not found
43: Consumption file unreadable
44: Consumption file bad format

