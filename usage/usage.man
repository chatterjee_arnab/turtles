turtles:₹ usage [command]

Gives a detailed description of the specified command's invocation syntax and usage.
If command argument is omitted it shows the complete list of all available commands.

Error codes
1: Command does not exist
2: Command manual missing

