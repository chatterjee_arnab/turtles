$ turtles IPv4_port IPv6_port [max_peers record_size mempool_size [min_fee]]
$ turtles command [options] [empty.file]
$ turtles [command] help
$ turtles version [commodity/consumption/document/network/transfer]

All turtles shell command names are case insensitive.
Command arguments are processed and matched with command syntax from left to right.

.hex  indicates the argument must be specified in hexadecimal (lower or upper case).
.b32  indicates the argument must be specified in base32 (lower case or upper case).
.txt  indicates the argument must be plain text representation of the required data.
.file indicates the argument must be a file path (filename must have an extension).
...   indicates that repetition is allowed for a type/group of command arguments.

turtles:₹ account [accnum.b32[accnum.b32]/account.hex/userid.b32]

turtles:₹ block [blockid.b32/height]

turtles:₹ clear

turtles:₹ exit

turtles:₹ folder [name.file hash_type] [documentid.b32 [word_offset [word_count]]]
turtles:₹ folder [name.file ...] hash_type [list.file] [[-]previous_docid.b32] [next_userid.b32] [consumptn.file]

turtles:₹ formula axscode.b32/axsdata.b32/axsform.b32/code.hex/infix.txt/postfix.txt/segpost.txt

turtles:₹ record recordid.b32[@index.hex]/blockid.b32@index.hex/height.b32@index.hex

turtles:₹ transfer transferid.b32[@OIindex.hex]
turtles:₹ transfer
[(transfer_amount payee_accnum.b32[accnum.b32/@map.hex]/account.hex/userid.b32
 [axsform.b32/axscode.b32/code.hex [axsdata.b32/data.hex]]) ...]
[(payer_accnum.b32/account.hex/userid.b32/transferid.b32@OICindex.hex [payment])...]
[outputs.file] [inputs.file] [consumption.file] [miner_fees]

turtles:₹ usage [command]

turtles:₹ wallet [-] [userid.b32/accnum.b32] [encryptedprivatekey.b32] [wallet.file]

