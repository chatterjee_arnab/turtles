turtles:₹ formula axscode.b32/axsdata.b32/axsform.b32/code.hex/infix.txt/postfix.txt/segpost.txt
Shows detailed description of the boolean formula specified in any of these formats.
axscode: Access code (32 bytes hash of code segment) in base32 (T/t) representation.
axsdata: Access data (32 bytes hash of data segment) in base32 (T/t) representation.
axsform: Access formula (32 bytes hash of boolean formula) in base32 representation.
code   : Binary code segment of boolean formula represented in hexadecimal encoding.
infix  : Boolean formula in infix form (all operands must be given in decimal form).
postfix: Boolean formula in postfix (each operand is made of 2 hexadecimal symbols).
segpost: Boolean formula in segmented postfix (each operand is of 2 hexdec symbols).

Error codes
50: Formula general error
51: AxsCode does not exist
52: AxsData does not exist
53: AxsForm does not exist
54: Infix form not valid
55: Postfix form not valid
56: SegPost form not valid

