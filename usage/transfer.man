turtles:₹ transfer transferid.b32[@OIindex.hex]
This syntax shows description of a transfer, or one of its output(O)/input(I) index.

turtles:₹ transfer
[(transfer_amount payee_accnum.b32[accnum.b32/@map.hex]/account.hex/userid.b32
 [axsform.b32/axscode.b32/code.hex [axsdata.b32/data.hex]]) ...]
[(payer_accnum.b32/account.hex/userid.b32/transferid.b32@OICindex.hex [payment])...]
[outputs.file] [inputs.file] [consumption.file] [miner_fees]
In online mode, this syntax initiates a new transfer of crypto-commodity payments.
In offline mode, it does a non-persistent simulation of the same functionality.
transfer_amount : Amount of bytes to be transferred to the payee (next argument).
payee_accnum    : Account number for the recipient of the transfer.
accnum          : Transfers to a composite account after merging with payee_accnum.
map             : Remap boolean variable indices to groups (only for this transfer).
account         : Binary account specification data encoded in hexadecimal form.
userid          : User ID (compressed public key). Specifies the default account.
axsform         : Access formula(32B hash of boolean formula) for securing transfer.
axscode         : Access code (32 bytes hash of code segment) for securing transfer.
code            : Encoded binary code segment specification for the boolean formula.
axsdata         : Access data (32 bytes hash of data segment) for securing transfer.
                  It may be specified only with axscode or code (not with axsform).
data            : Encoded binary data segment specification for the boolean formula.
                  It may be specified only with axscode or code (not with axsform).
payer_accnum    : Account number for the payer/authorizer/signer(s) of the transfer.
transferid      : Unexhausted (neither transferred nor consumed) input to transfer.
OICindex        : Selects an output(O)/input(I)/consumption(C) for transferid.
payment         : Amount in bytes to be debited from transferid. Excess is returned.
                  Any value more than transferid index amount indicates full amount.
outputs.file    : Recipients list in line separated format (same syntax as command).
inputs.file     : Isolated payers list in multiline format (same as command syntax).
                  If only one of inputs or consumption file is specified, then it is
                  considered to be inputs file (isolated payment signatures scheme).
consumption.file: Combined payers list in multiline format (same as command syntax).
                  If total requirement (payment + consumption) exceeds total debit
                  specified, an internal algorithm is invoked to select more inputs.
miner_fees      : Fee paid to miner. Same semantics as 'min_fee' of turtles command.

Error codes
70: Transfer general error
71: Amount bytes not valid
72: AccNum does not exist
73: Remapping incompatible
74: Account data not valid
75: AxsForm does not exist
76: AxsForm incompatible
77: AxsCode does not exist
78: AxsCode incompatible
79: Code segment incompatible
80: AxsData does not exist
81: AxsData incompatible
82: Data segment incompatible
83: AccNum not in wallet
84: Account not in wallet
85: Account insufficient balance
86: UserID not in wallet
87: User ID insufficient balance
88: Transfer inaccessible
89: OICindex is not valid
90: OICindex out of range
91: Output file not found
92: Output file unreadable
93: Output file bad format
94: Inputs file not found
95: Inputs file unreadable
96: Inputs file bad format
97: Consumption file not found
98: Consumption file unreadable
99: Consumption file bad format

