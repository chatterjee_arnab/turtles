#!/usr/bin/python3.9 -BWignore

from socket    import AF_INET, AF_INET6, inet_pton, socket

from sys       import argv

from threading import Thread

if not (len(argv) is 3 and argv[1].isdigit() and argv[2].isdigit()): exit(1)
port4, port6 = int(argv[1]), int(argv[2])
if not port4 or port4 >= 1<<16 or not port6 or port6 >= 1<<16: exit(2)

IPv4 = (\
["127.0.0.1", 2014],
["127.0.0.1", 2016],
["127.0.0.1", 2018],
)
IPv6 = (\
["::1", 2015],
["::1", 2017],
["::1", 2019],
)

count4, count6 = len(IPv4), len(IPv6)
if count4 > 15 or count6 > 15: exit(3)
nodelist = bytearray(1 + 6*count4 + 18*count6)
nodelist[0], index = count4<<4 | count6, 1

try:
	for address in IPv4:
		IP, port = address
		if not 0 < port < 1<<16: exit(4)
		address[0], address[1] = inet_pton(AF_INET , IP), port.to_bytes(2, "big")
	for address in IPv6:
		IP, port = address
		if not 0 < port < 1<<16: exit(4)
		address[0], address[1] = inet_pton(AF_INET6, IP), port.to_bytes(2, "big")
except OSError  : exit(5)
except TypeError: exit(6)

for address in IPv4:
	IP, port = address
	for offset in range( 4): nodelist[index + offset] = IP[offset]
	nodelist[index +  4], nodelist[index +  5] = port[0], port[1]
	index += 6
for address in IPv6:
	IP, port = address
	for offset in range(16): nodelist[index + offset] = IP[offset]
	nodelist[index + 16], nodelist[index + 17] = port[0], port[1]
	index += 18

socket4, socket6 = socket(), socket(AF_INET6)
socket4.bind(("", port4))
socket6.bind(("", port6))
socket4.listen(4)
socket6.listen(4)

def server(socket):
	while True:
		try:
			connection = socket.accept()[0]
			connection.sendall(nodelist)
			connection.close()
		except OSError: pass

Thread(target=server, args=(socket4,)).start()
server(socket6)
