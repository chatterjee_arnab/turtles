#!/usr/bin/python3 -B

if __name__ == "__main__":
	from sys import argv, path
	path[0] = ".."
	from service.init import init
	exit(init(argv))

from messages.usage import header

from os             import chmod

from pydoc          import pager

from sys            import path

def main(args):
	if len(args) > 2: return "bad_command_invocation"
	if len(args) is 1: manual_path = path[0] + "/usage/commands.man"
	else:
		args[1], manuals = args[1].lower(), ("account", "block", "folder", "formula", "record", "transfer", "turtles", "usage", "wallet")
		if args[1] not in manuals: return "command_does_not_exist"
		manual_path = path[0] + "/usage/" + args[1] + ".man"
	try:
		chmod(manual_path, 0o400)
		manual = open(manual_path)
	except FileNotFoundError: return "command_manual_missing"
	manpage = manual.read()
	manual.close()
	pager(header + manpage)
