#!/usr/bin/python3 -B

if __name__ == "__main__":
	from sys import argv, path
	path[0] = ".."
	from service.init import init
	exit(init(argv[1:]))

from database import database

from messages import help

import service.account
import service.block
import service.folder
import service.formula
import service.record
import service.transfer
import service.usage
import service.wallet

from utility.exit import status

def init(args, shell=False):
	args[0] = args[0].lower()
	if args[0].endswith(".py"): args[0] = args[0][args[0].rfind("/") + 1:-3]
	if len(args) is not 1 and args[1].lower() == "help":
		if len(args) is 2:
			message = getattr(help, args[0], None)
			if message is None: error = "command_does_not_exist"
			else:
				print(help.header + message)
				error = None
		else: error = "bad_command_invocation"
	else:
		command = getattr(service, args[0], None)
		if not command: error = "command_does_not_exist"
		else:
			if args[0] == "usage": shell = True
			if not shell: database.connect()
			error = command.main(args)
			if not shell: database.disconnect()
	if error:
		if args[0] in ("help", "password"): args[0] = "turtles"
		return status(args[0], error)
	return 0
