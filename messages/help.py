header   = "Command syntax (arguments within square brackets are optional)\n\n"

turtles  = "$ turtles IPv4_port IPv6_port [max_peers record_size [mempool_size [min_fee]]]\n"\
           "$ turtles command [options] [empty.file]\n"\
           "$ turtles [command] help\n"\
           "$ turtles version [commodity/consumption/document/network/transfer]"

account  = "\u20b9 account [accnum.b32[accnum.b32]/account.hex/userid.b32]"
block    = "\u20b9 block [blockid.b32/height]"
folder   = "\u20b9 folder [name.file hash_type] [documentid.b32 [word_offset [word_count]]]\n"\
           "\u20b9 folder [name.file ...] hash_type [list.file] [[-]previous_docid.b32] [next_userid.b32] [consumptn.file]"
formula  = "\u20b9 formula axscode.b32/axsdata.b32/axsform.b32/code.hex/infix.txt/postfix.txt/segpost.txt"
record   = "\u20b9 record recordid.b32[@index.hex]/blockid.b32@index.hex/recordid.b32@index.hex"
transfer = "\u20b9 transfer transferid.b32[@OIindex.hex]\n"\
           "\u20b9 transfer\n"\
           "[(transfer_amount payee_accnum.b32[accnum.b32/@map.hex]/account.hex/userid.b32\n"\
           " [axsform.b32/axscode.b32/code.hex [axsdata.b32/data.hex]]) ...]\n"\
           "[(payer_accnum.b32/account.hex/userid.b32/transferid.b32@OICindex.hex [payment])...]\n"\
           "[outputs.file] [inputs.file] [consumption.file] [miner_fees]"
usage    = "\u20b9 usage [command]"
wallet   = "\u20b9 wallet [-] [userid.b32/accnum.b32] [encryptedprivatekey.b32] [wallet.file]"
