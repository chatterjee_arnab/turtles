access_denied = "Access denied. Please recheck credentials in 'database/database.py'."
doesnot_exist = "Does not exist. Please create database and run 'scripts/tables.sql'."
no_connection = "No connection to server. Contact database/network/system admin."
read_failed   = "Read failed unexpectedly. Contact database/network/system admin."
write_failed  = "Write failed unexpectedly. Contact database/network/system admin."
commit_failed = "Commit failed unexpectedly. Contact database/network/system admin."

prefix = "Database error:"
