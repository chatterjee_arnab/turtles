record_does_not_exist  = "Record does not exist. Could not be found in the blockchain database."
block_does_not_exist   = "Block does not exist. Could not be found in local blockchain database."
block_height_too_large = "Block height too large. It must be less than local blockchain length."
Cindex_is_not_valid    = "Consumption index is not valid. Type 'usage record' for details."
Cindex_out_of_range    = "Consumption index out of range. Check index range by omitting index."

prefix = "Record error:"
