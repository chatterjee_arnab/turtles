account_not_parseable = "The account data specified could not be parsed. Please check it again."
accnum_does_not_exist = "This account number does not exist in the local blockchain database."
userid_does_not_exist = "The specified userID does not exist in the local blockchain database."

prefix = "Account error:"
