block_does_not_exist = "Block does not exist. Could not be found in local blockchain database."
block_height_too_big = "Block height too big. It must be less than the local blockchain length."

prefix = "Block error:"
