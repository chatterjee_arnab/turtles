command_does_not_exist = "Command does not exist. Type 'usage' to get list of available commands."
command_manual_missing = "Command manual missing in 'usage' directory. Download manual manually."

prefix = "Usage error:"

header = "Command usage (arguments within square brackets are optional)\n\n"
