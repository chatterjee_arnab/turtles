file_does_not_exist    = "File does not exist. Please check the input file path provided."
Nempty_file_unreadable = "Nonempty file is unreadable. Use 'chmod +r' to enable read permission."
hash_type_is_not_valid = "Hash type is not valid. Type 'usage folder' for available hash types."
import_file_corruption = "Import file is corrupted. Please recheck hash type or export it again."
documentid_not_found   = "The given document ID does not exist in the local blockchain database."
Woffset_is_not_valid   = "Specified word offset is not valid. It must be a non-negative integer."
Woffset_out_of_range   = "Word offset out of range for given document or one of its descendants."
wordcount_is_not_valid = "The specified word count is not valid. It must be a positive integer."
list_file_bad_format   = "Bad format for input filelist. Must be a line separated list of paths."
successor_not_allowed  = "Successor flag has been disabled for the specified previous document."
userid_is_not_valid    = "This is not a valid userID in base32 representation. Please try again."
consumption_not_found  = "Consumption file does not exist. Please create the file and try again."
consumption_unreadable = "Consumption file unreadable. Use 'chmod +r' to enable read permission."
consumption_bad_format = "Consumption file format must be same as transfer command input format."

prefix = "Folder error:"
