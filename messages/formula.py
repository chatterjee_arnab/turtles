axscode_does_not_exist = "The given access code does not exist in local blockchain database."
axsdata_does_not_exist = "The given access data does not exist in local blockchain database."
axsform_does_not_exist = "The given access formula does not exist in local blockchain database."
infix_form_not_valid   = "The given boolean formula in infix form is invalid. Please try again."
postfix_form_not_valid = "This boolean formula in postfix form is invalid. Please try again."
segpost_form_not_valid = "This formula in segmented postfix form is invalid. Please try again."

prefix = "Formula error:"
