incorrect = "Authentication failure due to incorrect password."
is_short  = "Required password length is at least 8 characters."
mismatch  = "Confirm password does not match with new password."
no_digit  = "Must contain at least one decimal digit character."
no_lower  = "Must contain at least one lower case letter [a-z]."
no_other  = "Must have at least one non-alphanumeric character."
no_upper  = "Must contain at least one upper case letter [A-Z]."

prefix    = "Password error:"
suffix    = "Please try again."
